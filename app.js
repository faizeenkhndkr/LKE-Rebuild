﻿(

    function() {

        var app = angular.module("MyLkeApp", ["ui.router", "ngMaterial", "720kb.datepicker", "ui-notification", "ui.grid", "ui.grid.autoResize", "ngIdle", "disableAll", "ngBootstrap", "ngMessages", "ui.utils.masks", "ui.bootstrap", "ui.grid.pagination", "ui.grid.expandable", "ui.grid.selection", "ui.grid.pinning", 'ui.grid.pagination', "toggle-switch"])

        .constant("appSetting", { FilePath: '', ServerPath: 'http://localhost:60789/', IsDebug: false }); //http://localhost:3000/

        app.config(function($stateProvider, $urlRouterProvider, appSetting, IdleProvider, KeepaliveProvider, $mdThemingProvider) {
            //http://localhost:60789/  https://lke-new-api-staging.azurewebsites.net/
            //http://localhost:3000/
            //https://lke-webapi.azurewebsites.net/    - testing
            // https://lke-new-staging.azurewebsites.net/#!/login - staging

            IdleProvider.idle(1800); // in seconds
            IdleProvider.timeout(1800); // in seconds
            KeepaliveProvider.interval(900); // in seconds
            $urlRouterProvider.otherwise('/login');

            $stateProvider.state("login", {
                url: "/login",
                templateUrl: appSetting.FilePath + "modules/Login/login.html",
                controller: "loginCtrl"
            });

            $stateProvider.state("dashboard", {
                url: "/dashboard",
                templateUrl: appSetting.FilePath + "modules/Dashboard/dashboard.html",
                controller: "dashboardCtrl"
            });

            $stateProvider.state("reports", {
                url: "/reports",
                templateUrl: appSetting.FilePath + "modules/Reports/reports.html",
                controller: "reportsCtrl"
            });
            $stateProvider.state("reports.details", {
                url: "/reportsDetail",
                templateUrl: appSetting.FilePath + "modules/Reports/reportsDetails.html",
                controller: "reportsDetailCtrl"
            });
            $stateProvider.state("45-Days-Ids", {
                url: "/45-Days-Ids",
                templateUrl: appSetting.FilePath + "modules/45-Day-Ids/45-day-ids.html",
                controller: "45DaysIdsCtrl"
            });
            $stateProvider.state("administration", {
                url: "/administration",
                templateUrl: appSetting.FilePath + "modules/Administration/AdminDashboard.html",
                controller: "adminDashboardCtrl"
            });
            $stateProvider.state("relinquishedAsset", {
                url: "/RelinquishedAsset",
                params: {
                    selectedAssetsList: null
                },
                templateUrl: appSetting.FilePath + "modules/45-Day-Ids/relinquishedAsset.html",
                controller: "relinquishedAssetCtrl"
            });
            $stateProvider.state("recentSubmissions", {
                url: "/RecentSubmissions",
                params: {
                    selectedAssetsList: null
                },
                templateUrl: appSetting.FilePath + "modules/45-Day-Ids/RecentSubmissions.html",
                controller: "recentSubmissionsCtrl"
            });
            $stateProvider.state("assetMaintenance", {
                url: "/assetMaintenance",
                templateUrl: appSetting.FilePath + "modules/AssetMaintenance/assterMaintance.html",
                controller: "assetMaintananceCtrl"
            });

            $stateProvider.state("changepassword", {
                url: "/changepassword",
                templateUrl: appSetting.FilePath + "modules/Administration/changePassword.html",
                controller: "changePasswordCtrl"
            });

            $stateProvider.state("reportManagement", {
                url: "/reportManagement",
                templateUrl: appSetting.FilePath + "modules/Administration/UpdateAutoArchive/ReportManagement.html",
                controller: "reportManagementCTRL"
            });

            $stateProvider.state("userManagement", {
                url: "/userManagement",
                templateUrl: appSetting.FilePath + "modules/Administration/UpdateAutoArchive/userManagement.html",
                controller: "userManagementCTRL"
            });
            // $stateProvider.state("TaxPayerInfo", {
            //     url: "/TaxPayerInfo",
            //     templateUrl: appSetting.FilePath + "modules/TaxPayerInfo/TaxPayerInformation.html",
            //     controller: "taxInfoCtrl"
            // });


        });
        app.controller("selectDatabase", ["$scope", "$http", "appSetting", function($scope, $http, appSetting) {
            $scope.items = [1, 2, 3, 4, 5, 6, 7];
            $scope.selectedItem;
            $scope.getSelectedText = function() {
                if ($scope.selectedItem !== undefined) {
                    return $scope.selectedItem;
                } else {
                    return "Select a Database";
                }
            };
        }]);


    }());