﻿﻿(
    function() {
        angular.module("MyLkeApp").controller("MainCtrl", ["$scope", "$rootScope", "$http", "appSetting", "$state",

            "LoginService", "ConsoleService", "$mdDialog",
            "$http", "NotificationService", "LoadingService", "Notification", "Idle", "$timeout",
            function($scope, $rootScope, $http, appSetting, $state, LoginService, ConsoleService, $mdDialog, $http, NotificationService, LoadingService, Notification, Idle, $timeout) {


                $rootScope.obj = {};
                //$rootScope.userData = {};
                $rootScope.activeTab1 = "active";
                $rootScope.activeTab2 = "";
                $rootScope.activeTab3 = "";
                $rootScope.activeTab4 = "";
                $rootScope.activeTab5 = "";
                $rootScope.activeTab6 = "";
                $rootScope.isCollapseAdmin = false;
                $scope.sideNavFlex = 20;
                $scope.hideSideNavText = true;
                $scope.sideNavWidth = 80;
                $rootScope.credentials = {};
                $rootScope.isDisabled = false;
                $scope.sideNavBar = "Close";
                $rootScope.isAdminCollapse = false;

                if ($rootScope.userData.username && $rootScope.userData.role && $rootScope.userData.SelectedDatabase && $rootScope.userData.SelectedDatabase.actualDbaseName) {
                    try {
                        NotificationService.updateNotifications();
                    } catch (ex) {
                        ConsoleService.error('calling notifications data', "no credentials");
                    }
                }

                $rootScope.obj.adminAccess = localStorage.getItem('adminAccess');

                $scope.$watch('userData.SelectedDatabase', function() {

                    if ($rootScope.userData.SelectedDatabase) {
                        LoginService.storeUpdatedSelectedDatabase($rootScope.userData.SelectedDatabase);
                        ConsoleService.log("database changed watch", $rootScope.userData.SelectedDatabase);
                        $scope.$broadcast('databaseChangedBroadcaseEvent', null);
                        try {
                            $scope.getEntityData();
                        } catch (Ex) {
                            ConsoleService.error("changed database MainCtrl ", Ex);
                        }
                    }

                });


                $scope.getEntityData = function() {
                    //LoadingService.showLoader();
                    var promise = LoginService.getEntityData();
                    promise.then(
                        function(payload) {
                            //LoadingService.removeLoader();
                            if (payload != null && payload.status) {
                                LoginService.setEntityData(payload);
                                NotificationService.updateNotifications();
                            }
                        },

                        function(errorPayload) {
                            //LoadingService.removeLoader();
                            ConsoleService.log('getEntityData', errorPayload);
                        });

                }


                $scope.openNotificationsMenu = function($mdOpenMenu, ev) {

                    originatorEv = ev;
                    $mdOpenMenu(ev);
                    ConsoleService.log("notifications open", null);
                };
                $scope.disableAdmin = function(){}
                $scope.notificationItemClicked = function(item) {
                    ConsoleService.log("notifications item Clicked", item);
                    var link = window.document.createElement("a");
                    link.download = "Report";
                    link.href = item.createdFilePath;
                    link.click();
                }
                $scope.hideSideNav = function(sideNavFlex) {
                    if($rootScope.isAdminCollapse){
                        $rootScope.isAdminCollapse = false;
                    }
                    if (sideNavFlex == 20) {
                        $scope.sideNavFlex = 5;
                        $scope.hideSideNavText = false;
                        $scope.sideNavWidth = 95;
                        $scope.sideNavBar = "Show";

                    }
                    if (sideNavFlex == 5) {
                        $scope.sideNavFlex = 20;
                        $scope.hideSideNavText = true;
                        $scope.sideNavWidth = 80;
                        $scope.sideNavBar = "Hide";
                    }
                }
                $scope.collapseAdminPannel = function(){
                    $rootScope.isAdminCollapse = !$rootScope.isAdminCollapse;
                    if($rootScope.isAdminCollapse){
                        $scope.sideNavFlex = 20;
                        $scope.hideSideNavText = true;
                        $scope.sideNavWidth = 80;
                        $scope.sideNavBar = "Show";
                    }
                }

                $scope.signoutClicked = function() {
                    $rootScope.credentials.username = localStorage.getItem('username');
                    $rootScope.credentials.password = localStorage.getItem('password');
                    $rootScope.credentials.role = localStorage.getItem('role');
                    $rootScope.credentials.adminAccess = localStorage.getItem('adminAccess');
                    $rootScope.credentials.rememberMe = localStorage.getItem('rememberMe');
                    LoginService.setLogout();
                    $rootScope.newReportsIn = false;
                    window.onpopstate = function(e) { window.history.forward(1); }
                }
                $scope.$on('IdleStart', function() {
                    ConsoleService.log("IdleStart", null);
                });

                $scope.$on('IdleWarn', function(e, countdown) {
                    ConsoleService.log("IdleWarn", null);
                });

                $scope.$on('IdleTimeout', function() {
                    ConsoleService.log("IdleTimeout", null);
                    $scope.sessionLogout();
                    $timeout(function() {
                        if (!$rootScope.isDisabled) {
                            LoginService.setLogout();
                        }
                    }, 120000);
                    $rootScope.isDisabled = false;
                });
                $scope.$on('IdleEnd', function() {
                    ConsoleService.log("IdleEnd", null);
                });

                $scope.$on('Keepalive', function() {
                    ConsoleService.log("Keepalive", null);
                });
                $scope.sessionEnded = function() {
                    $rootScope.isDisabled = false;
                    LoginService.setLogout();
                };
                $scope.sessionContinue = function() {
                    $rootScope.isDisabled = false;
                    Idle.watch();
                };
                $scope.wraningMessage = "You are about to be signed out of your session.";

                $scope.sessionLogout = function() {
                    Notification.primary({ message: "", templateUrl: "sessionAutoLogout_template.html", scope: $scope, delay: 115000 });
                    $rootScope.isDisabled = true;
                };



            }
        ]);


    } ());

