(function () {

    angular.module('MyLkeApp').factory('_45DaysIdsService', ["$log", "appSetting", "$q", "$rootScope", "$http", "ConsoleService", function ($log, appSetting, $q, $rootScope, $http, ConsoleService) {
        
            
            /*var someval = {};
            return someval;*/
            var upComing45DayDeadLines= function () {
                var deferred = $q.defer();
                $http.get(appSetting.ServerPath + "reports/get45DayIDDeadlines?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName + "&entityID=" + $rootScope.userData.EntityData.data.entityID + "&caseID=" + $rootScope.userData.EntityData.data.caseID + "&caseID2=" + $rootScope.userData.EntityData.data.caseID2)
                .then(function (responseDataFull) {
                    var responseData = responseDataFull.data;
                    ConsoleService.log('loadYearData',responseData);
                    if (responseData.status) {
                        deferred.resolve(responseData);
                    }
                    else {
                        deferred.resolve(null);
                    }
                }, function (error) {
                    ConsoleService.error('loadYearData',error);
                    deferred.resolve(null);
                });
                return deferred.promise;
            }

            return {
                upComing45DayDeadLines : upComing45DayDeadLines
            };
    }]
)
})();