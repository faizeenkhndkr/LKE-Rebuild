(function() {

    angular.module("MyLkeApp").service("NotificationService", ["appSetting", "$state", "ConsoleService", "$http", "$q", "$rootScope", "Notification",
            function(appSetting, $state, ConsoleService, $http, $q, $rootScope, Notification) {
                //$rootScope.newReportsIn = false;
                var firstLoad = true;
                this.updateNotifications = function() {
                    $http.get(appSetting.ServerPath + "reports/getNotifications?username=" + $rootScope.userData.username + "&dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName)
                        .then(function(responseDataFull) {
                            var responseData = responseDataFull.data;
                            ConsoleService.log('getNotifications Data', responseData);
                            if (responseData.status) {
                                $rootScope.notificationList = responseData.data;
                                if (!firstLoad) {
                                    try {
                                        if ($rootScope.notificationList.length > 0 && $rootScope.isLoggedIn) {
                                            Notification.success('Report(s) available');
                                        }
                                    } catch (ex) {

                                    }
                                } else {
                                    firstLoad = false;
                                }
                                //$rootScope.newReportsIn = true;
                            } else {
                                //$rootScope.newReportsIn = false;
                            }
                        }, function(error) {
                            ConsoleService.error('get notifications data', error);

                        });
                }


            }
        ]

    );

}());