(function() {

    angular.module("MyLkeApp").service("LoginService", ["appSetting", "$state", "ConsoleService", "$http", "$q", "$rootScope", "$window",
            function(appSetting, $state, ConsoleService, $http, $q, $rootScope, $window) {

                var isLoggedInLS = localStorage.getItem("isLoggedIn");

                if (isLoggedInLS != null) {
                    try {
                        ConsoleService.log("reload logging in", isLoggedInLS);
                        $rootScope.isLoggedIn = isLoggedInLS;
                    } catch (ex) {
                        ConsoleService.log("reload logging in", "error");
                    }
                }
                $rootScope.userData = {};
                var userDataLS = localStorage.getItem("userData");
                if (userDataLS != null) {
                    try {
                        ConsoleService.log("reload userData", userDataLS);
                        $rootScope.userData = JSON.parse(userDataLS);
                    } catch (ex) {
                        ConsoleService.log("reload userData", "error");
                    }
                }
                // var roles = localStorage.getItem("role");
                // if (roles != null) {
                //     try {
                //         ConsoleService.log("reload userData", roles);
                //         $rootScope.userData = JSON.parse(roles);
                //     } catch (ex) {
                //         ConsoleService.log("reload userData", "error");
                //     }
                // }


                var selectedDatabaseLS = localStorage.getItem("selectedDatabase");
                if (selectedDatabaseLS != null) {
                    try {
                        ConsoleService.log("reload selected database", selectedDatabaseLS);
                        $rootScope.userData.SelectedDatabase = JSON.parse(selectedDatabaseLS);
                        ConsoleService.log("userdata after reload", $rootScope.userData);
                    } catch (ex) {
                        ConsoleService.log("reload selected database", "error");
                    }
                }
                ConsoleService.log('', "reloaded");

                this.setLogout = function() {
                    $http.get(appSetting.ServerPath + "reports/removeNotifications?username=" + $rootScope.userData.username)
                        .then(function(success) {

                            window.onpopstate = function(e) { window.history.forward(1); }
                        }, function(error) {

                        });

                    localStorage.clear();
                    $rootScope.isLoggedIn = false;
                    $rootScope.userData = {};

                    $state.go("login");
                }

                this.Login = function(data) {
                    var deferred = $q.defer();
                    $http.post(appSetting.ServerPath + "UserLogin/AuthenticateUser", data)
                        .then(function(responseDataFull) {
                            var responseData = responseDataFull.data;
                            ConsoleService.log('login', responseData);
                            if (responseData.status) {
                                localStorage.setItem("isLoggedIn", true);
                                $rootScope.isLoggedIn = true;
                                if (responseData.role == 'S') {
                                    $rootScope.obj.adminAccess = true;
                                    localStorage.setItem('adminAccess', $rootScope.obj.adminAccess);
                                    ConsoleService.log("selected role", responseData.role);
                                } else {
                                    $rootScope.obj.adminAccess = false;
                                }
                                deferred.resolve(responseData);
                            } else {
                                deferred.resolve(responseData);
                            }
                        }, function(error) {
                            ConsoleService.error('login', error);
                            deferred.resolve(null);
                        });
                    return deferred.promise;
                }

                this.setUserData = function(userData) {
                    var isDatabaseSelected = false;
                    localStorage.setItem("userData", JSON.stringify(userData));
                    $rootScope.userData = userData;
                    if ($rootScope.userData.dbList.length == 1) {
                        $rootScope.userData.SelectedDatabase = $rootScope.userData.dbList[0];
                        isDatabaseSelected = true;
                    }
                    return isDatabaseSelected;
                }

                this.storeUpdatedSelectedDatabase = function(selectedDatabase) {

                    ConsoleService.log("storing database ", selectedDatabase);
                    localStorage.setItem("selectedDatabase", JSON.stringify(selectedDatabase));
                }

                this.setEntityData = function(payload) {
                    $rootScope.userData.EntityData = payload;
                    localStorage.setItem("userData", JSON.stringify($rootScope.userData));
                }

                this.getEntityData = function() {
                    try {
                        var deferred = $q.defer();
                        $http.get(appSetting.ServerPath + "UserLogin/GetEntityData?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName)
                            .then(function(responseDataFull) {
                                var responseData = responseDataFull.data;
                                ConsoleService.log('getEntityData', responseData);
                                if (responseData.status) {
                                    deferred.resolve(responseData);
                                } else {
                                    deferred.resolve(null);
                                }
                            }, function(error) {
                                ConsoleService.error('getEntityData', error);
                                deferred.resolve(null);
                            });
                        return deferred.promise;
                    } catch (error) {
                        ConsoleService.error('getEntityData', error);
                    }
                }

            }
        ]

    );
}());