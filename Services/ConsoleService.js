
// (function () {

//     angular.module("MyLkeApp").service("ConsoleService",["appSetting", function (appSetting) {
       
//         this.log = function (obj) {
//             if(appSetting.IsDebug)
//             {
//                 console.log(obj);
//             }
//         }  
//     }]
//     );
// } ())



(function () {

    angular.module('MyLkeApp').service('ConsoleService', ["$log", "appSetting", function ($log, appSetting) {
        this.log = function (methodName, data) {
            if (appSetting.IsDebug) {
                $log.debug('---------' + methodName + '------------');
                $log.debug(data);
            }
          
        }

        this.error = function (methodName, data) {
            if (appSetting.IsDebug) {
                $log.error('---------' + methodName + '------------');
                $log.error(data);
            }
        }
    }]
)
})();