
(function () {

    angular.module("MyLkeApp").service("LoadingService",
        ["$rootScope",
            function ($rootScope) {

                var loadingCounter = 0;

                this.showLoader = function () {
                    loadingCounter++;
                    $rootScope.globalloading = true;
                }

                this.removeLoader = function () {
                    loadingCounter--;
                   // if (loadingCounter == 0) {
                        $rootScope.globalloading = false;
                    //}
                }

            }]

    );
} ());