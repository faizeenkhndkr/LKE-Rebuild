
(function () {

    angular.module("MyLkeApp").service("ReportsDataService", ["appSetting", "$state", "ConsoleService", "$http", "$q", "$rootScope", function (appSetting, $state, ConsoleService, $http, $q, $rootScope) {
        var Reports = {};

        this.setReportsData = function (reportsData) {
            Reports = reportsData;
        }

        this.getSelectedReportsObject = function (selectedReportIdList) {
            var SelectedReports = {};
            SelectedReports.categoryOneList = [];
            SelectedReports.categoryTwoList = [];
            SelectedReports.categoryThreeList = [];
            //angular.copy(Reports,SelectedReports);
            try {
                for (var i = 0; i < Reports.categoryOneList.length; i++) {
                    if (selectedReportIdList.indexOf(Reports.categoryOneList[i].reportListID) >= 0) {
                        SelectedReports.categoryOneList.push(Reports.categoryOneList[i]);
                    }
                }
                for (var i = 0; i < Reports.categoryTwoList.length; i++) {
                    if (selectedReportIdList.indexOf(Reports.categoryTwoList[i].reportListID) >= 0) {
                        SelectedReports.categoryTwoList.push(Reports.categoryTwoList[i]);
                    }
                }
                for (var i = 0; i < Reports.categoryThreeList.length; i++) {
                    if (selectedReportIdList.indexOf(Reports.categoryThreeList[i].reportListID) >= 0) {
                        SelectedReports.categoryThreeList.push(Reports.categoryThreeList[i]);
                    }
                }
            }
            catch (ex) {
                ConsoleService.error('error in reports data service,getSelectedReportsObject method',ex );
            }

            return SelectedReports;

        }


        this.loadYearData = function () {
            //reports/getYears
             var deferred = $q.defer();
                $http.get(appSetting.ServerPath + "reports/getYears?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName)
                .then(function (responseDataFull) {
                    var responseData = responseDataFull.data;
                    ConsoleService.log('loadYearData',responseData);
                    if (responseData.status) {
                        deferred.resolve(responseData);
                    }
                    else {
                        deferred.resolve(null);
                    }
                }, function (error) {
                    ConsoleService.error('loadYearData',error);
                    deferred.resolve(null);
                });
            return deferred.promise;

         
        }

        this.loadAllTaxBooks = function () {
            var taxbooks = "FEDERAL, FBI";

            ConsoleService.log('loadAllTaxBooks',taxbooks);

            return taxbooks.split(/, +/g).map(function (taxbook) {
                return {
                    value: taxbook + "",
                    display: taxbook + ""
                };
            });
        }

        this.loadAllDepreciationTypes = function () {
            //reports/getDepTypes
             var deferred = $q.defer();
                $http.get(appSetting.ServerPath + "reports/getDepTypes?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName)
                .then(function (responseDataFull) {
                    var responseData = responseDataFull.data;
                    ConsoleService.log('loadAllDepreciationTypes',responseData);
                    if (responseData.status) {
                        deferred.resolve(responseData);
                    }
                    else {
                        deferred.resolve(null);
                    }
                }, function (error) {
                    ConsoleService.error('loadAllDepreciationTypes',error);
                    deferred.resolve(null);
                });
            return deferred.promise;


            // var depreciationTypes = "Alternate Minimum Tax, US Tax, US Tax - No Bonus";

            // ConsoleService.log(depreciationTypes);

            // return depreciationTypes.split(/, +/g).map(function (depreciationType) {
            //     return {
            //         value: depreciationType + "",
            //         display: depreciationType + ""
            //     };
            // });
        }


        this.getAllReportList = function () {
            var deferred = $q.defer();
            $http.get(appSetting.ServerPath + "reports/getUserReports?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName)
                .then(function (responseDataFull) {
                    var responseData = responseDataFull.data;
                    ConsoleService.log('getAllReportList',responseData);
                    if (responseData.status) {
                        deferred.resolve(responseData);
                    }
                    else {
                        deferred.resolve(null);
                    }
                }, function (error) {
                    ConsoleService.error('getAllReportList',error);
                    deferred.resolve(null);
                });
            return deferred.promise;
        }


        this.makeMultipleFileDownloadRequest = function (reqObj) {
            var deferred = $q.defer();
            $http.post(appSetting.ServerPath + "reports/getMultipleReports",reqObj)
                .then(function (responseDataFull) {
                    var responseData = responseDataFull.data;
                    ConsoleService.log('makeMultipleFileDownloadRequest',responseData);
                    if (responseData.status) {
                        deferred.resolve(responseData);
                    }
                    else {
                        deferred.resolve(null);
                    }
                }, function (error) {
                    ConsoleService.error('makeMultipleFileDownloadRequest',error);
                    deferred.resolve(null);
                });
            return deferred.promise;
        }


    }]

    );
} ());