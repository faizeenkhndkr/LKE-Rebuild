﻿(
    function() {
        angular.module("MyLkeApp").controller("dashboardCtrl", ["$scope", "$http", "appSetting",
            "$rootScope", "$state", "ReportsDataService", "LoginService", "LoadingService", "ConsoleService",
            "$filter", "$timeout", "uiGridConstants", "$q",
            function($scope, $http, appSetting, $rootScope, $state, ReportsDataService, LoginService,

                LoadingService, ConsoleService, $filter, $timeout, uiGridConstants, $q) {

                $rootScope.activeTab1 = "active";
                $rootScope.activeTab2 = "";
                $rootScope.activeTab3 = "";
                $rootScope.activeTab4 = "";
                $rootScope.activeTab5 = "";
                $rootScope.activeTab6 = "";
                $rootScope.isCollapseAdmin = false;
                $rootScope.isAdminCollapse = false;

                $scope.init = function() {
                    $scope.gridOptions = {
                        enableColumnMenus: false,
                        enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
                        enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,

                    };
                    $scope.gridOptions.columnDefs = [

                        { name: 'ASSET ID', displayName: 'ASSET ID', field: 'assetID', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.assetID}}</div>' },
                        { name: 'SALE DATE', displayName: 'SALE DATE', field: 'saleDate', cellTemplate: '<div style="text-align:center; margin-top:5px;">{{row.entity.saleDate | date:\'MM\/dd\/yyyy\'}}</div>' },
                        { name: 'SALE AMOUNT', displayName: 'SALE AMOUNT', field: 'salesPrice', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.salesPrice | currency}}</div>' },
                        { name: 'POTENTIAL GAIN', displayName: 'POTENTIAL GAIN', field: 'potentialGain', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.potentialGain | currency}}</div>' },
                        { name: 'ASSET CLASS', displayName: 'ASSET CLASS', field: 'assetCategory', cellTemplate: '<div style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.assetCategory}}</div>' },
                        { name: '45-DAY DEADLINE', displayName: '45-DAY DEADLINE', field: 'day45', cellTemplate: '<div style="text-align:center; margin-top:5px;">{{row.entity.day45 | date:\'MM\/dd\/yyyy\'}}</div>' },

                        { name: 'DAYS REMAINING', displayName: 'DAYS REMAINING', field: 'dayRemaining', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.dayRemaining}}</div>' },

                    ];

                    // $scope.gridOptions.data=[{
                    //     "assetid":"d2",
                    //     "saleDate":"d3",
                    //     "saleAmount":"dfjkhdfk",
                    //     "potentialGain":"dskjhdskds,",
                    //     "assetClass":"slsdj",
                    //     "day45":"dljd",
                    //     "daysRemaining":"dsljdl"
                    // },
                    // {
                    //     "assetid":"d2c",
                    //     "saleDate":"d3fdf",
                    //     "saleAmount":"dfjkhdfkddf",
                    //     "potentialGain":"dskjhdskdsdff,",
                    //     "assetClass":"slsdjdfs",
                    //     "day45":"dljddsds",
                    //     "daysRemaining":"dsljdldsds"
                    // }];
                    $scope.gridOptions1 = {
                        enableColumnMenus: false,
                        enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
                        enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
                    };

                    $scope.gridOptions1.columnDefs = [
                        { name: 'ASSET CATEGORY', displayName: 'ASSET CATEGORY', field: 'assetCategory', cellTemplate: '<div style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.assetCategory}}</div>' },
                        { name: 'SALES', displayName: 'SALES', field: 'sales', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.sales | currency}}</div>' },
                        { name: 'PURCHASES', displayName: 'PURCHASES', field: 'purchases', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 20px;">{{row.entity.purchases | currency}}</div>' },

                    ];
                    // $scope.gridOptions1.data=[{
                    //     "assetCategory":"d2",
                    //     "sales":"d3",
                    //     "purchases":"dfjkhdfk"
                    // },
                    // {
                    //     "assetCategory":"d2c",
                    //     "sales":"d3fdf",
                    //     "purchases":"dfjkhdfkddf"
                    // }];
                }
                $scope.getData = function() {
                    try {
                        LoadingService.showLoader();
                        $http.get(appSetting.ServerPath + "reports/salePurchaseActivityList?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName + "&entityID=" + $rootScope.userData.EntityData.data.entityID + "&caseID=" + $rootScope.userData.EntityData.data.caseID + "&caseID2=" + $rootScope.userData.EntityData.data.caseID2)
                            .then(function(responseDataFull) {
                                LoadingService.removeLoader();
                                var responseData1 = responseDataFull.data;
                                $scope.gridOptions1.data = responseData1.data;
                                ConsoleService.log('salePurchaseActivityList', responseData1);


                            }, function(error) {
                                ConsoleService.error('salePurchaseActivityList', error);
                            });
                    } catch (error) {
                        ConsoleService.error('salePurchaseActivityList', error);
                    }
                    try {
                        LoadingService.showLoader();
                        $http.get(appSetting.ServerPath + "reports/get45DayIDDeadlines?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName + "&entityID=" + $rootScope.userData.EntityData.data.entityID + "&caseID=" + $rootScope.userData.EntityData.data.caseID + "&caseID2=" + $rootScope.userData.EntityData.data.caseID2)
                            .then(function(responseDataFull) {
                                LoadingService.removeLoader();
                                var responseData2 = responseDataFull.data;
                                $scope.gridOptions.data = responseData2.data;
                                console.log($scope.gridOptions.data);
                                ConsoleService.log('get45DayIDDeadlines', responseData2);


                            }, function(error) {
                                ConsoleService.error('get45DayIDDeadlines', error);
                            });
                    } catch (error) {
                        ConsoleService.error('salePurchaseActivityList', error);
                    }

                }

                if ($rootScope.userData.username && $rootScope.userData.SelectedDatabase && $rootScope.userData.SelectedDatabase.actualDbaseName) {

                    $scope.getData();
                }

                $scope.$on('databaseChangedBroadcaseEvent', function(event, data) {
                    //console.log(data); // 'Some data'
                    try {
                        if ($rootScope.userData.username && $rootScope.userData.SelectedDatabase && $rootScope.userData.SelectedDatabase.actualDbaseName) {

                            $scope.getData()();
                        }
                    } catch (Ex) {

                    }
                });
            }
        ]);




    }());