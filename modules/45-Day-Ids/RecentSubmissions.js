(
    function() {

        angular.module("MyLkeApp").controller("recentSubmissionsCtrl", ["$scope", "$http", "appSetting",


            "uiGridConstants", "$state", "$rootScope", "ConsoleService", "$mdDialog", "LoadingService",
            function($scope, $http, appSetting, uiGridConstants, $state, $rootScope, ConsoleService, $mdDialog, LoadingService) {


                $scope.revokeEerrorMessage = false;

                if (!$rootScope.isLoggedIn) {
                    $state.go("login");
                }

                $scope.recentSubmissionsGrid = {
                    enableColumnMenus: false,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
                    //enableFiltering : true,
                    onRegisterApi: function(gridApi) {
                        $scope.gridApi = gridApi;
                    },
                };
                $scope.enableRecentlySubIdsFilter = function() {
                    $scope.recentSubmissionsGrid.enableFiltering = !$scope.recentSubmissionsGrid.enableFiltering;
                    $scope.gridApi.core.clearAllFilters();
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                }
                $scope.recentSubmissionsGrid.data = $state.params.selectedAssetsList;
                $scope.recentSubmissionsGrid.columnDefs = [
                    { name: 'ASSET ID', displayName: 'ASSET ID', field: 'assetID', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.assetID}}</div>' },
                    {
                        name: 'SALE DATE',
                        displayName: 'SALE DATE',
                        field: 'saleDate',
                        width: '145',
                        cellTemplate: '<div style="text-align:center; margin-top:5px;">{{row.entity.saleDate | date:\'MM\/dd\/yyyy\'}}</div>',
                        cellFilter: 'date:\'MM\/dd\/yyyy\'',
                        filterHeaderTemplate: '<input type="daterange" id="incorpdate" ng-model="daterange" maxDate="2022/12/31" minDate="1980/01/01" value=""  />'
                    },
                    {
                        name: '45-Day Deadline',
                        displayName: '45-DAY DEADLINE',
                        field: 'day45',
                        width: '145',
                        cellTemplate: '<div style="text-align:center; margin-top:5px;">{{row.entity.day45 | date:\'MM\/dd\/yyyy\'}}</div>',
                        cellFilter: 'date:\'MM\/dd\/yyyy\'',
                        filterHeaderTemplate: '<input type="daterange" id="incorpdate" ng-model="daterange" maxDate="2022/12/31" minDate="1980/01/01" value=""  />'
                    },
                    { name: 'DAYS REMAINING', displayName: 'DAYS REMAINING', field: 'dayRemaining', cellTemplate: '<div style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.dayRemaining}}</div>' },
                ];
                $scope.revokeInputChanged = function() {
                    if (!$scope.revokeSignature) {
                        $scope.revokeEerrorMessage = true;
                    } else {
                        $scope.revokeEerrorMessage = false;
                    }
                }

                $scope.revokeAssetIds = function(ev) {
                    if (!$scope.revokeSignature) {
                        //$scope.revokeEerrorMessage = true;

                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#dialogContainer3')))
                            .clickOutsideToClose(true)
                            .title('45-Day IDs')
                            .textContent('Please enter signature')
                            .ariaLabel('Please enter signature')
                            .ok('OK')
                            .targetEvent(ev)
                        );

                        return;
                    }
                    var finalObj = {};
                    finalObj.makeModelList = [];
                    finalObj.dbName = $rootScope.userData.SelectedDatabase.actualDbaseName;
                    finalObj.userID = $rootScope.userData.userID;
                    finalObj.username = $rootScope.userData.username;
                    finalObj.signature = "Test";
                    finalObj.description = "sample Description";
                    finalObj.revokeIDList = [];
                    angular.forEach($state.params.selectedAssetsList, function(value, key) {
                        finalObj.revokeIDList.push(value.id);
                    });

                    $http.post(appSetting.ServerPath + "45DayID/revokeIDs", finalObj)
                        .then(function(responseDataFull) {
                            var responseData = responseDataFull.data;
                            ConsoleService.log('revoke 45 day id data', responseData);
                            if (responseData.status) {
                                $state.go("45-Days-Ids");
                            } else {
                                ConsoleService.error('revoking failed', 'success false');
                            }
                        }, function(error) {
                            ConsoleService.error('revoking failed', error);
                        });
                }
            }
        ]);

    }());