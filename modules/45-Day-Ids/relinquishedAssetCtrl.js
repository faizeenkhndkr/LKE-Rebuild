(
    function() {

        angular.module("MyLkeApp").controller("relinquishedAssetCtrl", ["$scope", "$http", "appSetting",
            "uiGridConstants", "$state", "ConsoleService", "$mdDialog", "$interval", "$rootScope",

            "$filter", "LoadingService",
            function($scope, $http, appSetting,
                uiGridConstants, $state, ConsoleService, $mdDialog, $interval, $rootScope, $filter, LoadingService) {
                $scope.isAssetSelected = false;
                $scope.errorMessage = false;

                if (!$rootScope.isLoggedIn) {
                    $state.go("login");
                }

                $scope.relinquishedAssetGrid = {
                    enableColumnMenus: false,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
                    //enableFiltering : true,
                    onRegisterApi: function(gridApi) {
                        $scope.gridApi = gridApi;
                    },
                };

                $scope.relinquishedAssetGrid.data = $state.params.selectedAssetsList;
                $scope.enableRelenquishedAssetFilter = function() {
                    $scope.relinquishedAssetGrid.enableFiltering = !$scope.relinquishedAssetGrid.enableFiltering;
                    $scope.gridApi.core.clearAllFilters();
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                }
                $scope.relinquishedAssetGrid.columnDefs = [
                    { name: 'ASSET ID', displayName: 'ASSET ID', field: 'assetID', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; margin-top:5px; margin-left:5px;padding-left: 3px;">{{row.entity.assetID}}</div>' },
                    {
                        name: 'SALE DATE',
                        displayName: 'SALE DATE',
                        field: 'saleDate',
                        cellTemplate: '<div style="text-align:center; margin-top:5px;">{{row.entity.saleDate | date:\'MM\/dd\/yyyy\'}}</div>',
                        cellFilter: 'date:\'MM\/dd\/yyyy\'',
                        filterHeaderTemplate: '<input type="daterange" id="incorpdate" ng-model="saleDate" maxDate="2022/12/31" minDate="1980/01/01"  />'
                    },
                    { name: 'SALE AMOUNT', displayName: 'SALE AMOUNT', field: 'salesPrice', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.salesPrice | currency}}</div>' },
                    { name: 'POTENTIAL GAIN', displayName: 'POTENTIAL GAIN', field: 'potentialGain', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.potentialGain | currency}}</div>' },
                    { name: 'ASSET CLASS', displayName: 'ASSET CLASS', field: 'assetCategory', cellTemplate: '<div style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.assetCategory}}</div>' },
                    {
                        name: '45-Day Deadline',
                        displayName: '45-DAY DEADLINE',
                        field: 'day45',
                        cellTemplate: '<div style="text-align:center; margin-top:5px;">{{row.entity.day45 | date:\'MM\/dd\/yyyy\'}}</div>',
                        cellFilter: 'date:\'MM\/dd\/yyyy\'',
                        filterHeaderTemplate: '<input type="daterange" id="incorpdate" ng-model="saleDate" maxDate="2022/12/31" minDate="1980/01/01"  />'
                    },

                    { name: 'DAYS REMAINING', displayName: 'DAYS REMAINING', field: 'dayRemaining', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; margin-top:5px; margin-left:5px;padding-left: 3px;">{{row.entity.dayRemaining}}</div>' },

                ];
                $scope.replacementAssetGrid = {
                    enableColumnMenus: false,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,

                };

                $scope.replacementAssetGrid.columnDefs = [{
                        name: ' ',
                        displayName: ' ',
                        field: ' ',
                        headerCellTemplate: '<div class="input-overflow-ellipsis" style="text-align:center;"><a ng-click="grid.appScope.editRow()"><img style="cursor:pointer" src="./Images/add1.png"></a></div>',
                        width: 100,
                        cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:center;"><a  ng-click="grid.appScope.editRow(row.entity)"><img style="cursor:pointer"  src="./Images/edit.png" alt="Edit"></a> <a ng-click="grid.appScope.deleteRow(row.entity)"><img style="cursor:pointer"  src="./Images/delete.png" alt="Delete"></a> </div>'
                    },


                    { name: 'MAKE/MODEL CODE', displayName: 'MAKE/MODEL CODE', field: 'modelCode', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.modelCode}}</div>' },
                    { name: 'ASSET CLASS', displayName: 'ASSET CLASS', field: 'assetClass', cellTemplate: '<div style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.assetClass}}</div>' },
                    { name: 'FMV', displayName: 'FMV', field: 'fmv', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.fmv.toFixed(2) | currency: "$" : 0}}</div>' },
                    { name: 'QUANTITY', displayName: 'QUANTITY', field: 'quantity', cellTemplate: '<div style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.quantity}}</div>' },
                    { name: 'TOTAL FMV', displayName: 'TOTAL FMV', field: 'totalFmv', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.totalFmv | currency: "$" : 0}}</div>' },
                ];

                $scope.replacementAssetGrid.data = [
                    //{
                    //     modelCode: "dfdsfff",
                    //     assetClass: "dsfsdf",
                    //     fmv: 5124.00,
                    //     quantity: 1,
                    //     totalFmv: 12456
                    // },
                    // {
                    //     modelCode: "ddddd",
                    //     assetClass: "bbbbbbbb",
                    //     fmv: 2356.00,
                    //     quantity: 1,
                    //     totalFmv: 56451
                    // }
                    // {
                    //     assetClass:"333924",
                    //     fmv:0,
                    //     id:954,
                    //     modelCode:"MC/E6000-AC",
                    //     quantity:1,
                    //     totalFmv:0
                    // }
                    // ,
                    // {
                    //     assetClass:"333924",
                    //     fmv:0,
                    //     id:954,
                    //     modelCode:"MC/E6000-AC",
                    //     quantity:1,
                    //     totalFmv:0
                    // }
                ];



                $scope.deleteRow = function(selectedRow) {
                    ConsoleService.log('row selected for delete', selectedRow);
                    var list = $filter("filter")($scope.replacementAssetGrid.data, {
                        modelCode: selectedRow.modelCode,
                        assetClass: selectedRow.assetClass,
                        fmv: selectedRow.fmv,
                        quantity: selectedRow.quantity,
                        totalFmv: selectedRow.totalFmv
                    })
                    if (list.length > 0) {
                        for (var i = 0; i < $scope.replacementAssetGrid.data.length; i++) {
                            if ($scope.replacementAssetGrid.data[i].modelCode == selectedRow.modelCode &&
                                $scope.replacementAssetGrid.data[i].assetClass == selectedRow.assetClass &&
                                $scope.replacementAssetGrid.data[i].fmv == selectedRow.fmv &&
                                $scope.replacementAssetGrid.data[i].quantity == selectedRow.quantity &&
                                $scope.replacementAssetGrid.data[i].totalFmv == selectedRow.totalFmv) {
                                ConsoleService.log("removed object", $scope.replacementAssetGrid.data[i]);
                                $scope.replacementAssetGrid.data.splice(i, 1);
                                $scope.changeTotals();
                                break;
                            }
                        }

                    }
                    ConsoleService.log('row index selected for delete', $scope.replacementAssetGrid.data.indexOf(selectedRow));

                }

                //    modelCode: "dfdsfff",
                //     assetClass: "dsfsdf",
                //     fmv: 5124.00,
                //     quantity: 1,
                //     totalFmv: 12456

                $scope.editRow = function(selectedRow) {

                    ConsoleService.log("edit this row", selectedRow);
                    var confirm = $mdDialog.show({

                            scope: $scope,
                            preserveScope: true,
                            controller: DialogController,
                            preserveScope: true,
                            templateUrl: 'templates/editRelinquished.html',
                            // parent: angular.element(document.body),
                            // targetEvent: ev,
                            clickOutsideToClose: false,
                            locals: {
                                message: selectedRow
                            }
                        })
                        .then(function(object) {

                            ConsoleService.log('new Dialog Data', object.newData);
                            ConsoleService.log('old Dialog Data', object.oldData);
                            if (!object.isEdit) {
                                ConsoleService.log('add data', '');
                                $scope.replacementAssetGrid.data.push(object.newData);
                            } else {
                                $scope.deleteRow(object.oldData);
                                ConsoleService.log('edit data', '');
                                $scope.replacementAssetGrid.data.push(object.newData);
                            }
                            ConsoleService.log('replacement list after edit or add', $scope.replacementAssetGrid.data);
                            $scope.changeTotals();
                        }, function() {

                            ConsoleService.log('Dialog', 'You cancelled the dialog.');
                        });
                }

                $scope.quantitTotal = 0;
                $scope.totalFMVTotal = 0;

                $scope.changeTotals = function() {
                    $scope.quantitTotal = 0;
                    $scope.totalFMVTotal = 0;
                    angular.forEach($scope.replacementAssetGrid.data, function(value, key) {
                        $scope.quantitTotal = $scope.quantitTotal + value.quantity;
                        $scope.totalFMVTotal = $scope.totalFMVTotal + value.totalFmv;
                    })
                }


                $scope.checkForTreshold = function() {
                    $scope.tresholdAssetIds = [];
                    if ($scope.quantitTotal > 3) {

                        angular.forEach($scope.relinquishedAssetGrid.data, function(value, key) {
                            if ($scope.totalFMVTotal > (value.salesPrice * 2)) {
                                $scope.tresholdAssetIds.push(value.assetID);
                            }
                        })

                        if ($scope.tresholdAssetIds.length > 0) {
                            return true;
                        }
                    }
                    return false;
                }

                function DialogController($scope, $mdDialog, message, $rootScope) {


                    $scope.makeModObj = {};
                    $scope.oldData = {};
                    $scope.isEdit = false;
                    if (message != undefined) {
                        $scope.makeModObj = message;
                        $scope.modelSelected = $filter('filter')($scope.makeModelDropDownObj, { id: $scope.makeModObj.id })[0];
                        $scope.oldData = message;
                        $scope.isEdit = true;
                    } else {
                        $scope.modelSelected = null;
                    }
                    ConsoleService.log('data in controller', $scope.makeModObj);

                    $scope.hide = function() {
                        $mdDialog.hide();
                    };

                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };

                    $scope.submit = function(data) {
                        var sendObj = {};
                        sendObj.newData = data;
                        sendObj.oldData = $scope.oldData;
                        sendObj.isEdit = $scope.isEdit;
                        $mdDialog.hide(sendObj);
                    };
                    $scope.makeModelChange = function(selectedMakeModelObj, searchText) { //$scope.makeModelDropDownObj

                        ConsoleService.log('makeModelDropDown', selectedMakeModelObj);
                        for (var i = 0; i < $scope.makeModelDropDownObj.length; i++) {
                            if ($scope.makeModelDropDownObj[i].makeModelValue == searchText) {
                                $scope.isAssetSelected = false;
                                break;
                            } else {

                                $scope.isAssetSelected = true;
                            }
                        }
                        $scope.makeModObj.id = selectedMakeModelObj.id;
                        $scope.makeModObj.assetClass = selectedMakeModelObj.assetCategory;
                        $scope.makeModObj.modelCode = selectedMakeModelObj.make + "/" + selectedMakeModelObj.model;
                        if (!$scope.isEdit) {
                            $http.get(appSetting.ServerPath + "45DayID/getMakeModeIDData?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName + "&makeModeID=" + selectedMakeModelObj.id)
                                .then(function(responseDataFull) {
                                    var responseData = responseDataFull.data;
                                    ConsoleService.log('save 45 day id data', responseData);
                                    if (responseData.status) {

                                        if (responseData.data) {
                                            $scope.makeModObj.fmv = responseData.data.purPriceEst;
                                            $scope.makeModObj.quantity = responseData.data.quantityEstat;
                                            $scope.fmvORQuantityChanged();
                                        } else {
                                            ConsoleService.log(' data null', 'success true');
                                            $scope.makeModObj.fmv = 0;
                                            $scope.makeModObj.quantity = 1;
                                            $scope.fmvORQuantityChanged();
                                        }
                                    } else {
                                        ConsoleService.error('getmakemod data failed', 'success false');
                                        $scope.makeModObj.fmv = 0;
                                        $scope.makeModObj.quantity = 1;
                                        $scope.fmvORQuantityChanged();
                                    }
                                }, function(error) {
                                    ConsoleService.error('saving failed', error);
                                    $scope.makeModObj.fmv = 0;
                                    $scope.makeModObj.quantity = 1;
                                    $scope.fmvORQuantityChanged();
                                });
                        }
                    }


                    $scope.fmvORQuantityChanged = function() {
                        $scope.makeModObj.totalFmv = $scope.makeModObj.fmv * parseInt($scope.makeModObj.quantity);
                    }

                    $scope.getDataForMakemodelDropdown = function() {
                        try {
                            $http.get(appSetting.ServerPath + "/45DayID/getMakeModels?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName + "&assetCategory=" + $scope.relinquishedAssetGrid.data[0].assetCategory)
                                .then(function(responseDataFull) {
                                    var responseData = responseDataFull.data;
                                    $scope.makeModelDropDownObj = responseData.data;
                                    ConsoleService.log('makeModelDropDown', responseData.data);
                                }, function(error) {
                                    ConsoleService.error('makeModelDropDown', error);
                                });
                        } catch (error) {
                            ConsoleService.error('makeModelDropDown', error);
                        }
                    }

                    $scope.getDataForMakemodelDropdown();

                }




                $scope.replacementAssetGrid.data = [];

                $scope.subMitInputChanged = function(sub_signature) {
                    if (!sub_signature) {
                        $scope.errorMessage = true;
                    } else {
                        $scope.errorMessage = false;
                    }
                }


                $scope.submitBtnclicked = function(subSignature, ev) {

                    if ($scope.replacementAssetGrid.data.length == 0) {
                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('45-day IDs')
                            .textContent('Please add replacement assets.')
                            .ariaLabel('Alert Dialog')
                            .ok('OK')
                            .targetEvent(ev)
                        );
                        return;
                    }

                    if (!subSignature) {

                        //$scope.errorMessage = true;
                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#dialogContainer1')))
                            .clickOutsideToClose(true)
                            .title('45-Day IDs')
                            .textContent('Please enter signature')
                            .ariaLabel('Please enter signature')
                            .ok('OK')
                            .targetEvent(ev)
                        );

                        return;
                    }

                    if ($scope.replacementAssetGrid.data.length > 3) {

                        var isFMVPresent = true;
                        angular.forEach($scope.replacementAssetGrid.data, function(value, key) {
                            if (isFMVPresent == true) {
                                if (value.fmv <= 0) {
                                    isFMVPresent = false;
                                }
                            }
                        })

                        if (isFMVPresent == false) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                .parent(angular.element(document.querySelector('#popupContainer')))
                                .clickOutsideToClose(true)
                                .title('45-day IDs')
                                .textContent('Error: You have identified more than 3 replacement assets and must enter the FMV for each of the assets identified')
                                .ariaLabel('Alert Dialog')
                                .ok('OK')
                                .targetEvent(ev)
                            );
                            return;
                        }
                    }

                    if ($scope.checkForTreshold()) {
                        var tresholdAssetString = "";
                        angular.forEach($scope.tresholdAssetIds, function(value, key) {
                            if (key == 0) {
                                tresholdAssetString = value;
                            } else if (key == $scope.tresholdAssetIds.length - 1) {
                                tresholdAssetString = tresholdAssetString + " and " + value;
                            } else {

                                tresholdAssetString = tresholdAssetString + ", " + value;
                            }
                        })
                        var confirm = $mdDialog.confirm()
                            .title('LKE says:')
                            .textContent('Warning: You have identified more than 3 replacement assets and the total FMV exceeds 200%')
                            .ariaLabel('Alert')
                            .targetEvent(ev)
                            .ok('OK')
                            .cancel('Cancel');

                        $mdDialog.show(confirm).then(function() {

                            //$scope.submitServiceCall();


                        }, function() {

                        });
                    } else {
                        $scope.submitServiceCall();
                    }

                }


                $scope.submitServiceCall = function() {
                    var finalObj = {};
                    finalObj.makeModelList = [];
                    finalObj.dbName = $rootScope.userData.SelectedDatabase.actualDbaseName;
                    finalObj.userID = $rootScope.userData.userID;
                    finalObj.username = $rootScope.userData.username;
                    finalObj.signature = "Test";
                    finalObj.description = "sample Description";

                    angular.forEach($scope.relinquishedAssetGrid.data, function(asset, key) {
                        //ConsoleService.log("first table row", key + ': ' + value);
                        angular.forEach($scope.replacementAssetGrid.data, function(makeModel, key) {
                            var obj = {
                                'rowID': asset.id,
                                'quantity': makeModel.quantity,
                                'purchasePrice': makeModel.fmv,
                                'makeModID': makeModel.id
                            };
                            finalObj.makeModelList.push(obj);
                        })
                    });

                    ConsoleService.log("final obj for save", finalObj);


                    $http.post(appSetting.ServerPath + "45DayID/saveReplacementData", finalObj)

                    .then(function(responseDataFull) {
                        var responseData = responseDataFull.data;
                        ConsoleService.log('save 45 day id data', responseData);
                        if (responseData.status && !$scope.errorMessage) {
                            $state.go("45-Days-Ids");
                        } else {

                            ConsoleService.error('saving failed', 'success false');
                        }
                    }, function(error) {
                        ConsoleService.error('saving failed', error);
                    });
                }
            }
        ]);

    }());