(
    function() {

        angular.module("MyLkeApp").controller("45DaysIdsCtrl", ["$scope", "$http", "appSetting",


            "uiGridConstants", "$state", "$rootScope", "ConsoleService", "_45DaysIdsService", "$filter", "$mdDialog", "LoadingService",
            function($scope, $http, appSetting,
                uiGridConstants, $state, $rootScope, ConsoleService, _45DaysIdsService, $filter, $mdDialog, LoadingService) {

                $rootScope.activeTab1 = "";
                $rootScope.activeTab2 = "";
                $rootScope.activeTab3 = "";
                $rootScope.activeTab4 = "active";
                $rootScope.activeTab5 = "";
                $rootScope.activeTab6 = "";
                $rootScope.isCollapseAdmin = false;
                $rootScope.isAdminCollapse = false;


                if (!$rootScope.isLoggedIn) {
                    $state.go("login");
                }

                $scope.obj = {};
                $scope.obj.selectAll = false;
                $scope.objTable2 = {};
                $scope.obj.selectAllTable2 = false;
                $scope.is45RowSelected = false;
                $scope.isRecentlySubRowSel = false;
                $scope.selectedAssetIdRows = [];
                $scope.recentlySubmittedRow = [];
                $scope.upComingDeadLinesIDs = {
                    enableColumnMenus: false,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
                    onRegisterApi: function(gridApi) {
                        $scope.gridApi = gridApi;
                    },
                };
                $scope.enableUpComingDeadLinesFilter = function() {
                    $scope.upComingDeadLinesIDs.enableFiltering = !$scope.upComingDeadLinesIDs.enableFiltering;
                    if ($scope.upComingDeadLinesIDs.enableFiltering) {
                        $scope.getUpcomingDeadLinesData();
                    }
                    $scope.gridApi.core.clearAllFilters();
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                }
                $scope.upComingDeadLinesIDs.columnDefs = [
                    { name: 'checkBox', width: 50, cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:center;margin-left: -9px;"><input ng-click="grid.appScope.rowClicked(row.entity)" ng-checked="grid.appScope.isChecked(row.entity)" type="checkbox"></div>', headerCellTemplate: '<div><div class="ui-grid-vertical-bar">&nbsp;</div><div class="ui-grid-cell-contents"><input style="margin-left: -8px; vertical-align: middle" type="checkbox" ng-click="grid.appScope.selectAllRows()" ng-model="grid.appScope.obj.selectAll"></div></div>' },
                    { name: 'ASSET ID', displayName: 'ASSET ID', field: 'assetID', cellClass: 'ui-grid-vcenter', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.assetID}}</div>' },
                    {
                        name: 'SALE DATE',
                        displayName: 'SALE DATE',
                        field: 'saleDate',
                        width: '130',
                        cellTemplate: '<div style="text-align:center; margin-top:5px;">{{row.entity.saleDate | date:\'MM\/dd\/yyyy\'}}</div>',
                        cellFilter: 'date:\'MM\/dd\/yyyy\'',
                        filterHeaderTemplate: '<input type="daterange" id="incorpdate" ng-model="daterange" maxDate="2022/12/31" minDate="1980/01/01" value=""  />'
                    },
                    { name: 'SALE AMOUNT', displayName: 'SALE AMOUNT', field: 'salesPrice', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.salesPrice | currency}}</div>' },
                    { name: 'POTENTIAL GAIN', displayName: 'POTENTIAL GAIN', field: 'potentialGain', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.potentialGain | currency}}</div>' },
                    { name: 'ASSET CLASS', displayName: 'ASSET CLASS', field: 'assetCategory', cellTemplate: '<div style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.assetCategory}}</div>' },

                    {
                        name: '45-Day Deadline',
                        displayName: '45-DAY DEADLINE',
                        field: 'day45',
                        cellTemplate: '<div style="text-align:center; margin-top:5px;">{{row.entity.day45 | date:\'MM\/dd\/yyyy\'}}</div>',
                        cellFilter: 'date:\'MM\/dd\/yyyy\'',
                        filterHeaderTemplate: '<input type="daterange" id="incorpdate" ng-model="daterange" maxDate="2022/12/31" minDate="1980/01/01" value=""  />'
                    },

                    { name: 'DAYS REMAINING', displayName: 'DAYS REMAINING', field: 'dayRemaining', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; margin-top:5px;padding-left: 3px;">{{row.entity.dayRemaining}}</div>' },

                ];

                // $scope.date = {
                //     startDate: moment().subtract(1, "days"),
                //     endDate: moment()
                // };
                // $scope.singleDate = moment();

                // $scope.opts = {
                //     locale: {
                //         applyClass: 'btn-green',
                //         applyLabel: "Apply",
                //         fromLabel: "From",
                //         format: "YYYY-MM-DD",
                //         toLabel: "To",
                //         cancelLabel: 'Cancel',
                //         customRangeLabel: 'Custom range'
                //     },
                //     ranges: {
                //         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                //         'Last 30 Days': [moment().subtract(29, 'days'), moment()]
                //     }
                // };

                // $scope.setStartDate = function () {
                //     $scope.date.startDate = moment().subtract(4, "days").toDate();
                // };

                // $scope.setRange = function () {
                //     $scope.date = {
                //         startDate: moment().subtract(5, "days"),
                //         endDate: moment()
                //     };
                // };

                // //Watch for date changes
                // $scope.$watch('date', function (newDate) {
                //     console.log('New date set: ', newDate);
                // }, false);

                $scope.getUpcomingDeadLinesData = function() {
                    try {
                        LoadingService.showLoader();
                        $http.get(appSetting.ServerPath + "reports/get45DayIDDeadlines?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName + "&entityID=" + $rootScope.userData.EntityData.data.entityID + "&caseID=" + $rootScope.userData.EntityData.data.caseID + "&caseID2=" + $rootScope.userData.EntityData.data.caseID2)

                        .then(function(responseDataFull) {
                            LoadingService.removeLoader();
                            var responseData2 = responseDataFull.data;
                            $scope.upComingDeadLinesIDs.data = responseData2.data;
                            ConsoleService.log('gridOptions45', responseData2);


                        }, function(error) {
                            ConsoleService.error('gridOptions45', error);
                        });
                    } catch (error) {
                        ConsoleService.error('salePurchaseActivityList', error);
                    }
                }

                $scope.getRecentlySubmittedIdsData = function() {
                    LoadingService.showLoader();
                    $http.get(appSetting.ServerPath + "45DayID/getRecentlySubmitted?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName + "&entityID=" + $rootScope.userData.EntityData.data.entityID + "&caseID=" + $rootScope.userData.EntityData.data.caseID + "&caseID2=" + $rootScope.userData.EntityData.data.caseID2)
                        .then(function(responseDataFull) {
                            LoadingService.removeLoader();
                            var responseData = responseDataFull.data;
                            $scope.gridRecentlySubmittedIds.data = responseData.data;
                            ConsoleService.log('gridRecentlySubmittedIds', responseData);


                        }, function(error) {
                            ConsoleService.error('gridRecentlySubmittedIds', error);
                        });
                }
                $scope.getRecentlySubmittedIdsData();
                if ($rootScope.userData.username && $rootScope.userData.SelectedDatabase && $rootScope.userData.SelectedDatabase.actualDbaseName) {

                    $scope.getUpcomingDeadLinesData();
                }

                $scope.$on('databaseChangedBroadcaseEvent', function(event, data) {
                    try {
                        if ($rootScope.userData.username && $rootScope.userData.SelectedDatabase && $rootScope.userData.SelectedDatabase.actualDbaseName) {

                            $scope.getUpcomingDeadLinesData();
                            $scope.getRecentlySubmittedIdsData();
                        }
                    } catch (Ex) {

                    }
                });

                $scope.gridRecentlySubmittedIds = {
                    enableColumnMenus: false,
                    enableSorting: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
                    onRegisterApi: function(gridApi) {
                        $scope.gridApi1 = gridApi;

                    },
                };

                $scope.enableRecentlySubmittedFilter = function() {
                    $scope.gridRecentlySubmittedIds.enableFiltering = !$scope.gridRecentlySubmittedIds.enableFiltering;
                    if ($scope.gridRecentlySubmittedIds.enableFiltering) {
                        $scope.getRecentlySubmittedIdsData();
                    }
                    $scope.gridApi1.core.clearAllFilters();
                    $scope.gridApi1.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                }

                $scope.gridRecentlySubmittedIds.columnDefs = [{

                        name: ' ',
                        width: 50,
                        cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:center;margin-left: -9px;"><input ng-click="grid.appScope.RecentlySubmittedIDclicked(row.entity)" ng-checked="grid.appScope.isCheckedTable2(row.entity)" type="checkbox"></div>',
                        headerCellTemplate: '<div><div class="ui-grid-vertical-bar">&nbsp;</div><div class="ui-grid-cell-contents"><input style="margin-left: -8px; vertical-align: middle" type="checkbox" ng-click="grid.appScope.selectAllRowsTable2()" ng-model="grid.appScope.objTable2.selectAllTable2"></div></div>'
                    },
                    { name: 'ASSET ID', displayName: 'ASSET ID', field: 'assetId', width: '23%', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; margin-top:5px; margin-left:5px;">{{row.entity.assetID}}</div>' },

                    {
                        name: 'SALE DATE',
                        displayName: 'SALE DATE',
                        field: 'saleDate',
                        width: '24%',
                        cellTemplate: '<div style="text-align:center; margin-top:5px;">{{row.entity.saleDate | date:\'MM\/dd\/yyyy\'}}</div>',
                        cellFilter: 'date:\'MM\/dd\/yyyy\'',
                        filterHeaderTemplate: '<input type="daterange" id="incorpdate" ng-model="daterange" maxDate="2022/12/31" minDate="1980/01/01" value=""  />'
                    },
                    {
                        name: '45-Day Deadline',
                        displayName: '45-DAY DEADLINE',
                        field: 'day45',
                        width: '24%',
                        cellTemplate: '<div style="text-align:center; margin-top:5px;">{{row.entity.day45 | date:\'MM\/dd\/yyyy\'}}</div>',
                        cellFilter: 'date:\'MM\/dd\/yyyy\'',
                        filterHeaderTemplate: '<input type="daterange" id="incorpdate" ng-model="daterange" maxDate="2022/12/31" minDate="1980/01/01" value=""  />'
                    },

                    { name: 'DAYS REMAINING', displayName: 'DAYS REMAINING', enableSorting: true, field: 'dayRemaining', width: '24%', cellTemplate: '<div style="text-align:left; margin-top:5px; margin-left:5px;">{{row.entity.dayRemaining}}</div>' },


                ];

                $scope.selectAllRows = function() {
                    $scope.is45RowSelected = !$scope.obj.selectAll;
                    if ($scope.selectedAssetIdRows.length > 0) {
                        $scope.selectedAssetIdRows = [];
                        $scope.obj.selectAll = false;
                    } else {
                        angular.copy($scope.upComingDeadLinesIDs.data, $scope.selectedAssetIdRows);
                        $scope.obj.selectAll = true;
                    }
                    ConsoleService.log("select all", $scope.obj.selectAll);

                }

                $scope._45DayIDClicked = function(ev) {
                    if ($scope.selectedAssetIdRows.length > 0) {
                        var assetClass = "";
                        var differentAssetCategory = false;
                        angular.forEach($scope.selectedAssetIdRows, function(value, key) {
                            if (assetClass.length == 0) {
                                assetClass = value.assetCategory;
                            } else if (value.assetCategory != assetClass) {
                                differentAssetCategory = true;
                            }
                        });
                        if (differentAssetCategory) {
                            // var confirm = $mdDialog.confirm()
                            //     .title('LKE says:')
                            //     .textContent('Warning: The assets selected for identification do not all have the same asset class assigned')
                            //     .ariaLabel('Alert')
                            //     .targetEvent(ev)
                            //     .ok('OK')
                            //     .cancel('Cancel');

                            // $mdDialog.show(confirm).then(function () {
                            //     $state.go("relinquishedAsset", { selectedAssetsList: $scope.selectedAssetIdRows });
                            // }, function () {

                            // });

                            $mdDialog.show(
                                $mdDialog.alert()
                                .parent(angular.element(document.querySelector('#dialogContainer')))
                                .clickOutsideToClose(true)
                                .title('LKE says:')
                                .textContent('Warning: The assets selected for identification do not all have the same asset class assigned')
                                .ariaLabel('Warning')
                                .ok('OK')
                                .targetEvent(ev)
                            );
                        } else {
                            $state.go("relinquishedAsset", { selectedAssetsList: $scope.selectedAssetIdRows });
                        }
                    } else {
                        $scope.is45RowSelected = true;
                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#dialogContainer')))
                            .clickOutsideToClose(true)
                            .title('45-Day IDs')
                            .textContent('Please select Asset IDs')
                            .ariaLabel('Please select Asset IDs')
                            .ok('OK')
                            .targetEvent(ev)
                        );
                    }
                }
                $scope.revokeBtnClicked = function(ev) {
                    ConsoleService.log("$scope.recentlySubmittedRow", $scope.recentlySubmittedRow);
                    if ($scope.recentlySubmittedRow.length > 0) {
                        $state.go("recentSubmissions", { selectedAssetsList: $scope.recentlySubmittedRow });
                    } else {
                        $scope.isRecentlySubRowSel = true;
                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#dialogContainer1')))
                            .clickOutsideToClose(true)
                            .title('45-Day IDs')
                            .textContent('Please select Asset IDs')
                            .ariaLabel('Please select Asset IDs')
                            .ok('OK')
                            .targetEvent(ev)
                        );

                    }

                }
                $scope.isChecked = function(selectedRow) {
                    var list = $filter("filter")($scope.selectedAssetIdRows, { id: selectedRow.id })
                    if (list.length > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
                $scope.rowClicked = function(selectedRow) {
                    $scope.is45RowSelected = false;
                    var list = $filter("filter")($scope.selectedAssetIdRows, { id: selectedRow.id })
                    if (list.length > 0) {
                        for (var i = 0; i < $scope.selectedAssetIdRows.length; i++) {
                            if ($scope.selectedAssetIdRows[i].id == selectedRow.id) {
                                $scope.selectedAssetIdRows.splice(i, 1);
                                ConsoleService.log("removed object", $scope.selectedAssetIdRows[i]);
                                $scope.obj.selectAll = false;
                                break;
                            }
                        }

                    } else {

                        $scope.selectedAssetIdRows.push(selectedRow);
                        if ($scope.selectedAssetIdRows.length == $scope.upComingDeadLinesIDs.data.length) {
                            $scope.obj.selectAll = true;
                        }
                    }
                    ConsoleService.log("radio array", $scope.selectedAssetIdRows.length);
                    ConsoleService.log("select all", $scope.obj.selectAll);
                }

                $scope.RecentlySubmittedIDclicked = function(selectedRow) {
                    $scope.isRecentlySubRowSel = false;
                    var list = $filter("filter")($scope.recentlySubmittedRow, { id: selectedRow.id })
                    if (list.length > 0) {
                        for (var i = 0; i < $scope.recentlySubmittedRow.length; i++) {
                            if ($scope.recentlySubmittedRow[i].id == selectedRow.id) {
                                $scope.recentlySubmittedRow.splice(i, 1);
                                ConsoleService.log("removed object", $scope.recentlySubmittedRow[i]);
                                $scope.objTable2.selectAllTable2 = false;
                                break;
                            }
                        }

                    } else {

                        $scope.recentlySubmittedRow.push(selectedRow);
                        if ($scope.recentlySubmittedRow.length == $scope.gridRecentlySubmittedIds.data.length) {
                            $scope.objTable2.selectAllTable2 = true;
                        }
                    }
                    ConsoleService.log("radio array", $scope.recentlySubmittedRow.length);
                    ConsoleService.log("select all", $scope.objTable2.selectAllTable2);

                }
                $scope.isCheckedTable2 = function(selectedRow) {
                    var list = $filter("filter")($scope.recentlySubmittedRow, { id: selectedRow.id })
                    if (list.length > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
                $scope.selectAllRowsTable2 = function() {
                        $scope.isRecentlySubRowSel = !$scope.objTable2.selectAllTable2;
                        if ($scope.recentlySubmittedRow.length > 0) {
                            $scope.recentlySubmittedRow = [];
                            $scope.objTable2.selectAllTable2 = false;
                        } else {
                            angular.copy($scope.gridRecentlySubmittedIds.data, $scope.recentlySubmittedRow);
                            $scope.objTable2.selectAllTable2 = true;
                        }
                        ConsoleService.log("select all", $scope.objTable2.selectAllTable2);

                    }
                    /*$scope.showCustom = function(event) {
                        $mdDialog.show({
                            clickOutsideToClose: true,
                            scope: $scope,        
                            preserveScope: true,           
                            template: '<md-dialog>' +
                                        '  <md-dialog-content>' +
                                        '     Welcome to TutorialsPoint.com' +
                                        '  </md-dialog-content>' +
                                        '</md-dialog>',
                            controller: function DialogController($scope, $mdDialog) {
                                $scope.closeDialog = function() {
                                    $mdDialog.hide();
                                }
                            }
                        });
                    };*/

            }
        ]);

    }());