﻿(
    function() {


        angular.module("MyLkeApp").controller("loginCtrl", ["$scope", "$http", "appSetting", "$rootScope", "$state", "LoginService",
            "ConsoleService", "LoadingService", "NotificationService", "Idle",
            function($scope, $http, appSetting, $rootScope, $state, LoginService,
                ConsoleService, LoadingService, NotificationService, Idle) {

                $rootScope.errLoginMsg = false;
                $scope.LoginData = {};
                if ($rootScope.credentials.rememberMe) {
                    $scope.LoginData.username = $rootScope.credentials.username;
                    $scope.LoginData.password = $rootScope.credentials.password;
                    $scope.LoginData.role = $rootScope.credentials.role;
                    $scope.rememberMe = true;
                }

                //$scope.SelectedDatabase = "";
                $scope.loginButtonClicked = function(loginDataObj) {
                    if ($scope.rememberMe) {
                        localStorage.setItem('username', loginDataObj.username);
                        localStorage.setItem('password', loginDataObj.password);
                        //localStorage.setItem('role', loginDataObj.role);
                        localStorage.setItem('rememberMe', $scope.rememberMe);

                    }
                    if (!$rootScope.isLoggedIn) {
                        LoadingService.showLoader();
                        var promise = LoginService.Login(loginDataObj);
                        promise.then(
                            function(payload) {

                                LoadingService.removeLoader();
                                if (payload) {

                                    $scope.LoginResponse = payload;
                                    if ($scope.LoginResponse.status) {
                                        $scope.errLoginMsg = true;
                                        if (!payload.passwordUpdatedOn) {
                                            $state.go("changepassword");
                                        }
                                    } else {
                                        $scope.errLoginMsg = false;
                                    }

                                    var isDBSelected = LoginService.setUserData(payload);
                                    if (isDBSelected && payload.passwordUpdatedOn) {
                                        try {
                                            if ($rootScope.userData.SelectedDatabase.actualDbaseName.length > 0) {
                                                //$state.go("dashboard");
                                                $scope.getEntityData();
                                                //Idle.watch();
                                            } else {
                                                $scope.LoginResponse.displayMessage = "Please select a database";
                                                $rootScope.errLoginMsg = true;
                                            }

                                        } catch (ex) {

                                            $scope.LoginResponse.displayMessage = "Please select a database";
                                            $rootScope.errLoginMsg = true;
                                        }

                                    }
                                } else {
                                    $rootScope.errLoginMsg = true;
                                }

                            },

                            function(errorPayload) {
                                LoadingService.removeLoader();
                                $rootScope.errLoginMsg = true;
                                ConsoleService.log('loginButtonClicked', errorPayload);
                            });
                    } else {
                        try {
                            if ($rootScope.userData.SelectedDatabase.actualDbaseName.length > 0) {
                                //$state.go("dashboard");
                                $scope.getEntityData();
                            } else {
                                $scope.LoginResponse.displayMessage = "Please select a database";
                                $rootScope.errLoginMsg = true;
                            }
                        } catch (ex) {
                            $scope.LoginResponse.displayMessage = "Please select a database";
                            $rootScope.errLoginMsg = true;
                        }

                    }
                }


                $scope.getEntityData = function() {
                    LoadingService.showLoader();
                    var promise = LoginService.getEntityData();
                    promise.then(
                        function(payload) {
                            LoadingService.removeLoader();
                            if (payload != null && payload.status) {
                                LoginService.setEntityData(payload);
                                $state.go("dashboard");
                                Idle.watch();
                                // NotificationService.updateNotifications();
                            }
                        },

                        function(errorPayload) {
                            LoadingService.removeLoader();
                            ConsoleService.log('getEntityData', errorPayload);
                        });

                }

                $scope.$watch('LoginData.username', function() {
                    $scope.LoginResponse = {};
                    LoginService.setLogout();

                });
                $scope.$watch('LoginData.password', function() {
                    $scope.LoginResponse = {};
                    LoginService.setLogout();
                });
                $scope.$watch('LoginData.role', function() {
                    $scope.LoginResponse = {};
                    LoginService.setLogout();

                });

            }
        ]);


    }());