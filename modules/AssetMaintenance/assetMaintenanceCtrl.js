(
    function() {

        angular.module("MyLkeApp").controller("assetMaintananceCtrl", ["$scope", "$http", "appSetting", "$rootScope", "uiGridConstants", "ConsoleService", "$uibModal", "$mdDialog", "$filter", "LoadingService", "$state",
            function($scope, $http, appSetting, $rootScope, uiGridConstants, ConsoleService, $uibModal, $mdDialog, $filter, LoadingService, $state) {
                $rootScope.activeTab1 = "";
                $rootScope.activeTab2 = "";
                $rootScope.activeTab3 = "";
                $rootScope.activeTab4 = "";
                $rootScope.activeTab5 = "active";
                $rootScope.activeTab6 = "";
                $rootScope.isCollapseAdmin = false;
                $rootScope.isAdminCollapse = false;

                if (!$rootScope.isLoggedIn) {
                    $state.go("login");
                }

                $scope.obj = {};

                $scope.isCheckedLkeEligible = true;
                $scope.isCheckedLkeElPurchase = true;
                $scope.isCheckedRltdPartySale = false;
                $scope.isCheckedRltdPartyPurchase = false;
                $scope.noOfRows = 10;
                $scope.gridOptionsAssetMaintenance = {
                    enableColumnMenus: false,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
                    //enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
                    //useExternalPagination: true,
                    //enablePaginationControls: true,
                    //paginationPageSize: 9,
                    // paginationPageSizes: [10, 20, 30],
                    // paginationPageSize: 10,
                    expandableRowTemplate: 'modules/AssetMaintenance/subGrid.html',
                    expandableRowHeight: 79,
                    //expandableRowHeaderWidth: 50,
                    //subGridVariable will be available in subGrid scope
                    // expandableRowScope: {
                    //     subGridVariable: 'subGridScopeVariable'
                    // },
                    enableSorting: true,
                    onRegisterApi: function(gridApi) {
                        $scope.gridApi12 = gridApi;

                    },
                    // useCustomPagination: true

                };

                $scope.gridOptionsAssetMaintenance.onRegisterApi = function(gridApi) {
                    $scope.gridApi2 = gridApi;
                    $scope.gridApi2.core.refresh();
                    /*$scope.gridApi2.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        console.log('pager get data: pageNumber, pageSize', pageNumber, pageSize);
                        getPager(pageNumber, pageSize); 
                    });
                    */
                }

                $scope.definingAssetMaintenanceGridOptions = function() {
                    $scope.gridOptionsAssetMaintenance.columnDefs = [
                        // { name: 'EDIT', displayName: 'EDIT', width: 40, field: '', enableSorting: false, cellTemplate: '<div style="text-align:center; margin-top:5px;"><img src="./Images/edit.png" ng-click="grid.appScope.editPopup(row.entity)"/></div>' },
                        { name: 'ASSET ID', width: 70, displayName: 'ASSET ID', field: 'assetID', cellTemplate: '<div style="text-align:left;padding-left: 3px;">{{row.entity.assetID}}</div>' },
                        { name: "MAKE/MODEL", displayName: 'MAKE/MODEL', width: 220, field: "makeModel", cellTemplate: '<div style="text-align:left; margin-right:6px;padding-left: 3px;">{{row.entity.makeModel}}</div>' },
                        { name: "ASSET CLASS", displayName: 'ASSET CLASS', width: 80, field: "assetMakeModelID", cellTemplate: '<div style="text-align:left; margin-right:6px;padding-left: 3px;">{{row.entity.assetMakeModelID}}</div>' },
                        { name: "PURCHASE DATE", displayName: 'PURCHASE DATE', field: "parentAcqDate", cellTemplate: '<div style="text-align:center; margin-right:6px;">{{row.entity.parentAcqDate | date : "dd-MM-y"}}</div>' },
                        { name: "DATE IN SERVICE", displayName: 'DATE IN SERVICE', field: "dateInService", cellTemplate: '<div style="text-align:center; margin-right:6px;">{{row.entity.dateInService | date : "dd-MM-y"}}</div>' },
                        { name: "PURCHASE PRICE", displayName: 'PURCHASE PRICE', field: "acqCost", cellTemplate: '<div style="text-align:right; margin-right:6px;">{{row.entity.acqCost | currency : "$" : 2}}</div>' },
                        { name: 'LKE ELIGIBLE PURCHASE', width: 90, enableSorting: false, displayName: 'LKE ELIGIBLE PURCHASE', field: 'lkeEligible_Purchase', cellTemplate: '<div style="text-align:center; margin-right:6px;"><span ng-if="row.entity.lkeEligible_Purchase">YES</span><span ng-if="!row.entity.lkeEligible_Purchase">NO</span></div>' },
                        { name: "SALE DATE", displayName: 'SALE DATE', field: "saleDate", cellTemplate: '<div style="text-align:center; margin-right:6px;">{{row.entity.saleDate | date : "dd-MM-y"}}</div>' },
                        { name: "SALE PRICE", displayName: 'SALE PRICE', field: "salesPrice", cellTemplate: '<div style="text-align:right; margin-right:6px;">{{row.entity.salesPrice | currency : "$" : 2}}</div>' },
                        {
                            name: 'LKE ELIGIBLE SALE',
                            displayName: 'LKE ELIGIBLE SALE',
                            field: 'lkeEligible',
                            width: 100,
                            enableSorting: false,
                            cellTemplate: '<div style="text-align:center; margin-right:6px;"><span ng-if="row.entity.lkeEligible">YES</span><span ng-if="!row.entity.lkeEligible">NO</span></div>'
                        }

                        // { name: 'LKE ELIGIBLE (PURCHASE)', width: 75, enableSorting: false, displayName: 'LKE ELIGIBLE PURCHASE', field: 'lkeEligible_Purchase', cellTemplate: '<div style="text-align:center; margin-right:6px; margin-top: 5px;  "><span ng-if="row.entity.lkeEligible_Purchase">YES</span><span ng-if="!row.entity.lkeEligible_Purchase">NO</span></div>' },

                        // { name: 'LIABILITY RELIEVED', displayName: 'LIABILITY RELIEVED', field: 'liabilityRelieved', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.liabilityRelieved | currency : "$" : 2}}</div>' },
                        // { name: 'LIABILITY ASSUMED', displayName: 'LIABILITY ASSUMED', field: 'liabilityAssumed', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.liabilityAssumed | currency : "$" : 2}}</div>' },


                        // { name: 'BOOT', displayName: 'BOOT', field: 'bootReceived', cellTemplate: '<div style="text-align:right; margin-top:5px;padding-right: 3px;">{{row.entity.bootReceived | currency : "$" : 2}}</div>' },



                        // {
                        //     name: 'RELATED PARTY SALE',
                        //     displayName: 'RELATED PARTY SALE',
                        //     field: 'relatedPartySale',
                        //     width: 75,
                        //     enableSorting: false,
                        //     cellTemplate: '<div style="text-align:center; margin-right:6px; margin-top: 5px;  "><span ng-if="row.entity.relatedPartySale">YES</span><span ng-if="!row.entity.relatedPartySale">NO</span></div>'
                        // },
                        // {
                        //     name: 'RELATED PARTY PURCHASE',
                        //     displayName: 'RELATED PARTY PURCHASE',
                        //     field: 'relatedPartyPurchase',
                        //     enableSorting: false,
                        //     width: 95,
                        //     cellTemplate: '<div style="text-align:center; margin-right:6px; margin-top: 5px;  "><span ng-if="row.entity.relatedPartyPurchase">YES</span><span ng-if="!row.entity.relatedPartyPurchase">NO</span></div>'
                        // }
                    ];
                }
                $scope.backupData = [];
                $scope.definingAssetMaintenanceGridOptions();
                $rootScope.selectedRow = {};

                $scope.gridOptionsAssetMaintenance.onRegisterApi = function(gridApi) {
                    //set gridApi on scope
                    $scope.gridApi = gridApi;
                    //gridApi.rowEdit.on.getAssetMaitenanceData($scope, $scope.getAssetMaitenanceData);
                };

                $scope.serchAssetMaintenance = function(assetID, saleStartDate, saleEndDate, purchaseStartDate, purchaseEndDate) {

                    LoadingService.showLoader();
                    var withDates;
                    if (assetID && !saleStartDate && !saleEndDate && !purchaseStartDate && !purchaseEndDate) {
                        withDates = "&assetID=" + assetID + "&saleStartDate&saleEndDate&acqStartDate&acqEndDate";

                    } else if (!assetID && (saleStartDate && saleEndDate) && (!purchaseStartDate && !purchaseEndDate)) {
                        withDates = "&assetID=" + "" + "&saleStartDate=" + $filter('date')(new Date(saleStartDate), 'y-MM-dd') + "&saleEndDate=" + $filter('date')(new Date(saleEndDate), 'y-MM-dd') + "&acqStartDate=" + null + "&acqEndDate=" + null;
                        ConsoleService.log("sas", "sss");
                    } else if (!assetID && (!saleStartDate && !saleEndDate) && (purchaseStartDate && purchaseEndDate)) {
                        withDates = "&assetID=" + "&saleStartDate=" + "&saleEndDate=" + "&acqStartDate=" + $filter('date')(new Date(purchaseStartDate), 'y-MM-dd') + "&acqEndDate=" + $filter('date')(new Date(purchaseEndDate), 'y-MM-dd') + "";

                    }
                    $http.get(appSetting.ServerPath + "assetMaintanance/getAssetMaintananceList?dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName + withDates)
                        .then(function(data) {
                            //$scope.obj = {};
                            LoadingService.removeLoader();
                            for (i = 0; i < data.data.data.length; i++) {
                                var list = [{

                                    "assetID": data.data.data[i].assetID,
                                    "makeModel": data.data.data[i].makeModel,
                                    "assetMakeModelID": data.data.data[i].assetMakeModelID,
                                    "parentAcqDate": data.data.data[i].parentAcqDate,
                                    "acqCost": data.data.data[i].acqCost,
                                    "saleDate": data.data.data[i].saleDate,
                                    "salesPrice": data.data.data[i].salesPrice
                                }]
                                data.data.data[i].subGrid = {
                                    columnDefs: [{ name: "ASSET ID", width: 70, displayName: 'ASSET ID', field: "assetID", cellTemplate: '<div style="text-align:left; margin-right:6px;padding-left: 3px;">{{row.entity.assetID}}</div>' },
                                        { name: "MAKE/MODEL", displayName: 'MAKE/MODEL', width: 280, field: "makeModel", cellTemplate: '<div style="text-align:left; margin-right:6px;padding-left: 3px;">{{row.entity.makeModel}}</div>' },
                                        { name: "ASSET CLASS", width: 110, displayName: 'ASSET CLASS', field: "assetMakeModelID", cellTemplate: '<div style="text-align:left; margin-right:6px;padding-left: 3px;">{{row.entity.assetMakeModelID}}</div>' },
                                        { name: "PURCHASE DATE", displayName: 'PURCHASE DATE', field: "parentAcqDate", cellTemplate: '<div style="text-align:center; margin-right:6px;">{{row.entity.parentAcqDate | date : "dd-MM-y"}}</div>' },
                                        { name: "PURCHASE COST", displayName: 'PURCHASE COST', field: "acqCost", cellTemplate: '<div style="text-align:right; margin-right:6px;">{{row.entity.acqCost | currency : "$" : 2}}</div>' },
                                        { name: "SALE DATE", displayName: 'SALE DATE', field: "saleDate", cellTemplate: '<div style="text-align:center; margin-right:6px;">{{row.entity.saleDate | date : "dd-MM-y"}}</div>' },
                                        { name: "SALE PRICE", displayName: 'SALE PRICE', field: "salesPrice", cellTemplate: '<div style="text-align:right; margin-right:6px;">{{row.entity.salesPrice | currency : "$" : 2}}</div>' }
                                    ],
                                    data: list,
                                    enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
                                    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
                                    enableColumnMenus: false,
                                    rowHeight: 35,
                                    onRegisterApi: function(gridApi) {
                                        $scope.gridApi1 = gridApi;

                                    }
                                }
                            }
                            $scope.gridOptionsAssetMaintenance.data = data.data.data;
                            $scope.backupData = data.data.data;
                        }, function(error) {
                            ConsoleService.error('salePurchaseActivityList', error);
                        });
                }
                $scope.editPopup = function(selectedRow) {
                    selectedRow.obj = $scope.obj;
                    var confirm = $mdDialog.show({
                            scope: $scope,
                            preserveScope: true,
                            controller: assetMaintenanceDialogController,
                            templateUrl: 'templates/editAssetMaintanence.html',
                            clickOutsideToClose: false,
                            locals: {
                                message: selectedRow
                            }
                        })
                        .then(function(object) {}, function() {

                            ConsoleService.log('Dialog', 'You cancelled the dialog.');
                        });
                }

                function assetMaintenanceDialogController($scope, $mdDialog, message, $rootScope) {
                    $scope.searchData = message.obj;
                    delete message.obj;
                    $scope.assetMaintenance = {};
                    var assetMaintenanceObj;
                    if (message != undefined) {
                        ConsoleService.log('new Dialog Data', message);
                        assetMaintenanceObj = message;
                        angular.copy(assetMaintenanceObj, $scope.assetMaintenance);
                    } else {
                        assetMaintenanceObj = null;
                    }
                    $scope.hide = function() {
                        $mdDialog.hide();
                    };

                    $scope.cancel = function() {
                        ConsoleService.log('new Dialog Data', message);
                        $mdDialog.cancel();
                    };

                    $scope.submit = function(editedAssetMaintenance) {
                        for (var i = 0; i < $scope.gridOptionsAssetMaintenance.data.length; i++) {
                            if ($scope.gridOptionsAssetMaintenance.data[i].assetID == editedAssetMaintenance.assetID) {
                                $scope.gridOptionsAssetMaintenance.data[i] = assetMaintenanceObj;
                            }
                        }
                        $scope.updateAssetsObj = {};
                        $scope.updateAssetsObj = {
                            "dbname": $rootScope.userData.SelectedDatabase.actualDbaseName,
                            "AssetID": editedAssetMaintenance.assetID,
                            "LKEEligible": editedAssetMaintenance.lkeEligible,
                            "LKEEligible_Purchase": editedAssetMaintenance.lkeEligible_Purchase,
                            "LiabilityRelieved": editedAssetMaintenance.liabilityRelieved,
                            "LiabilityAssumed": editedAssetMaintenance.liabilityAssumed,
                            "RelatedPartySale": editedAssetMaintenance.relatedPartySale,
                            "RelatedPartyPurchase": editedAssetMaintenance.relatedPartyPurchase,
                            "BootReceived": editedAssetMaintenance.bootReceived,
                            "Dep_Type": editedAssetMaintenance.dep_Type
                        }
                        ConsoleService.log("post Asset", $scope.updateAssetsObj);
                        $http.post(appSetting.ServerPath + "assetMaintanance/updateAssetMaintananceList", $scope.updateAssetsObj)
                            .then(function(responseDataFull) {
                                var responseData = responseDataFull.data;
                                ConsoleService.log('$scope.updateAssetsObj', responseData);
                                if (responseData.status) {
                                    ConsoleService.log('data posted', 'success true');
                                    $scope.serchAssetMaintenance($scope.searchData.assetID, $scope.searchData.saleStartDate, $scope.searchData.saleEndDate, $scope.searchData.purchaseStartDate, $scope.searchData.purchaseEndDate);
                                    $mdDialog.cancel();
                                } else {
                                    ConsoleService.error('data not posted', 'success false');
                                }
                            }, function(error) {
                                ConsoleService.error('data posted failed', error);
                            });
                    };

                }
            }
        ]);


    }());