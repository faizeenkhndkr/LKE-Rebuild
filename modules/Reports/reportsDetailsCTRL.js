﻿(
    function() {

        angular.module("MyLkeApp").controller("reportsDetailCtrl", ["$scope", "$http", "appSetting",
            "$rootScope", "ReportsDataService", "ConsoleService", "LoginService", "LoadingService", "$http",
            "$interval", "$filter", "$timeout", "NotificationService", "Notification", "$state",
            function($scope, $http, appSetting, $rootScope, ReportsDataService, ConsoleService,
                LoginService, LoadingService, $http, $interval, $filter, $timeout, NotificationService, Notification, $state) {


                $rootScope.activeTab1 = "";
                $rootScope.activeTab2 = "";
                $rootScope.activeTab3 = "active";
                $rootScope.activeTab4 = "";
                $rootScope.activeTab5 = "";
                $rootScope.activeTab6 = "";

                if (!$rootScope.isLoggedIn) {
                    $state.go("login");
                }

                $scope.startDateKey = "StartDate";
                $scope.endDateKey = "EndDate";
                $scope.caseIDKey = "CaseID";
                $scope.caseID2Key = "CaseID2";
                $scope.LEIDKey = "LEID";
                $scope.depTypeKey = "Dep_Type";

                $scope.reportObject = {};
                $scope.reportObject.reconciliationReportSelecter = 'taxyear';

                //model for depreciation type
                $scope.selectedYearForTaxReports = -1;
                $scope.selectedDepTypeForTaxReports = -1;

                // list of `state` value/display objects

                $scope.taxBooks = ReportsDataService.loadAllTaxBooks();
                $scope.preSelectTaxYear = function(yearsList) {

                    try {
                        $scope.preSelectedTaxYear = $filter('filter')(yearsList, { display: (new Date().getFullYear()) - 1 })[0];
                        $scope.reportObject.selectedItemTaxYearLMR = $scope.preSelectedTaxYear;
                        $scope.reportObject.selectedItemTaxYearTR = $scope.preSelectedTaxYear;
                        $scope.reportObject.selectedItemTaxYearRR = $scope.preSelectedTaxYear;
                    } catch (error) {
                        ConsoleService.error('preSelectTaxYear', error);
                    }

                };

                $scope.preSelectDepType = function(depreciationTypeList) {
                    try {
                        $scope.preSelectedDepreciationType = $filter('filter')(depreciationTypeList, { value: "US_Tax" })[0];
                        $scope.reportObject.selectedItemDepTypeTR = $scope.preSelectedDepreciationType;
                        $scope.reportObject.selectedItemDepTypeRR = $scope.preSelectedDepreciationType;
                    } catch (error) {
                        ConsoleService.error('preSelectDepType', error);
                    }

                };

                //download depreciation types
                $scope.downloadYearsList = function() {
                    LoadingService.showLoader();
                    var _Promise = ReportsDataService.loadYearData();
                    _Promise.then(
                        function(payload) {
                            LoadingService.removeLoader();
                            if (payload != null) {
                                $scope.years = payload.data;
                                $scope.preSelectTaxYear($scope.years);
                            }
                        },
                        function(errorPayload) {
                            LoadingService.removeLoader();
                            ConsoleService.error('downloadYearsList', errorPayload);
                        });
                }();
                //download depreciation types
                $scope.downloadDepTypes = function() {
                    LoadingService.showLoader();
                    var _Promise = ReportsDataService.loadAllDepreciationTypes();
                    _Promise.then(
                        function(payload) {
                            LoadingService.removeLoader();
                            if (payload != null) {
                                $scope.depreciationTypes = payload.data;
                                $scope.preSelectDepType($scope.depreciationTypes);
                            }
                        },
                        function(errorPayload) {
                            LoadingService.removeLoader();
                            ConsoleService.error('downloadDepTypes', errorPayload);
                        });
                }();
                ConsoleService.log('year', $scope.years);
                ConsoleService.log("$scope.depreciationTypes", $scope.depreciationTypes);
                ConsoleService.log('taxBooks', $scope.taxBooks);
                ConsoleService.log('selectedReportList', $scope.selectedReportIdList);

                if ($scope.selectedReportIdList.length == 0) {
                    $state.go("reports");
                }

                //getting selected reports from previous screen
                $scope.SelectedReports = ReportsDataService.getSelectedReportsObject($scope.selectedReportIdList);
                ConsoleService.log('selectedReporst', $scope.SelectedReports);
                //Third List Events Start
                //Tax Year Change Event For LMR 
                $scope.selectedItemChangeTaxYearLMR = function(item) {
                    try {
                        $scope.selectedItemTaxYearLMR = item;
                        ConsoleService.log('before selectedItemTaxYearLMR', $scope.selectedItemTaxYearLMR);
                        $scope.selectedItemTaxYearLMR.startDate = $filter('date')(new Date($scope.selectedItemTaxYearLMR.startDate.replace(/-/g, '\/').replace(/T.+/, '')), 'MM/dd/yyyy');
                        $scope.selectedItemTaxYearLMR.endDate = $filter('date')(new Date($scope.selectedItemTaxYearLMR.endDate.replace(/-/g, '\/').replace(/T.+/, '')), 'MM/dd/yyyy');
                        ConsoleService.log('after selectedItemTaxYearLMR', $scope.selectedItemTaxYearLMR);
                    } catch (error) {
                        ConsoleService.error('selectedItemChangeTaxYearLMR', error);
                    }

                };
                //Third List Events End


                //Second List Events
                //Tax Year Change Event For LMR 

                $scope.selectedItemChangeTaxYearTR = function(item) {
                    try {
                        $scope.selectedItemTaxYearTR = item;
                        ConsoleService.log(' before selectedItemChangeTaxYearTR', $scope.selectedItemTaxYearTR);
                        $scope.selectedItemTaxYearTR.startDate = $filter('date')(new Date($scope.selectedItemTaxYearTR.startDate.replace(/-/g, '\/').replace(/T.+/, '')), 'MM/dd/yyyy');
                        $scope.selectedItemTaxYearTR.endDate = $filter('date')(new Date($scope.selectedItemTaxYearTR.endDate.replace(/-/g, '\/').replace(/T.+/, '')), 'MM/dd/yyyy');
                        ConsoleService.log(' after selectedItemChangeTaxYearTR', $scope.selectedItemTaxYearTR);
                    } catch (error) {
                        ConsoleService.error('selectedItemChangeTaxYearTR', error);
                    }
                };

                $scope.selectedItemChangeDepTypeTR = function(item) {
                    try {
                        $scope.selectedItemDepTypeTR = item;
                        ConsoleService.log('selectedItemChangeDepTypeTR', $scope.selectedItemDepTypeTR);
                    } catch (error) {
                        ConsoleService.error('selectedItemChangeDepTypeTR', error);
                    }

                };
                //Second List Events End



                //First List Events Start
                $scope.selectedItemChangeTaxYearRR = function(item) {
                    try {
                        $scope.selectedItemTaxYearRR = item;
                        ConsoleService.log('before selectedItemChangeTaxYearRR', $scope.selectedItemTaxYearRR);
                        $scope.selectedItemTaxYearRR.startDate = $filter('date')(new Date($scope.selectedItemTaxYearRR.startDate.replace(/-/g, '\/').replace(/T.+/, '')), 'MM/dd/yyyy');
                        $scope.selectedItemTaxYearRR.endDate = $filter('date')(new Date($scope.selectedItemTaxYearRR.endDate.replace(/-/g, '\/').replace(/T.+/, '')), 'MM/dd/yyyy');
                        ConsoleService.log('after selectedItemChangeTaxYearRR', $scope.selectedItemTaxYearRR);
                    } catch (error) {
                        ConsoleService.error('selectedItemChangeDepTypeTR', error);
                    }
                }

                $scope.selectedItemChangeDepTypeRR = function(item) {
                    try {
                        $scope.selectedItemDepTypeRR = item;
                    } catch (error) {
                        ConsoleService.error('selectedItemChangeDepTypeTR', error);
                    }
                }


                //Fist List Events End
                $scope.downloadSelectedReports = function(obj) {
                    //LoadingService.showLoader();
                    //$rootScope.newReportsIn = false;
                    ConsoleService.log("selected database ", $rootScope.userData.SelectedDatabase);
                    ConsoleService.log('userData', $rootScope.userData);
                    ConsoleService.log('selected reports', $scope.SelectedReports);
                    var downloadRequestData = {
                        "dB_Name": $rootScope.userData.SelectedDatabase.actualDbaseName,
                        "username": $rootScope.userData.username,
                        "entityName": $rootScope.userData.EntityData.data.entityName,
                        "fullName": $rootScope.userData.fullName,
                        "userEmailID": $rootScope.userData.userEmailID,
                        "userID": $rootScope.userData.userID
                    };
                    downloadRequestData.reportList = [];

                    // category Three list start
                    angular.forEach($scope.SelectedReports.categoryThreeList, function(selectedObj, key) {
                        switch (selectedObj.reportListID) {
                            case 12:
                                var obj = {};
                                obj = {
                                    "reportID": selectedObj.reportListID,
                                    "reportName": selectedObj.linkText,
                                    "reportShortName": selectedObj.outputName,
                                    "reportParams": {
                                        "0": 'US Tax'
                                    },
                                    "templateUrl": selectedObj.excelTemplateName
                                };
                                downloadRequestData.reportList.push(obj);
                                break;

                            case 1:
                                var obj = {};

                                obj = {
                                    "reportID": selectedObj.reportListID,
                                    "reportName": selectedObj.linkText,
                                    "reportShortName": selectedObj.outputName,
                                    "reportParams": {

                                        "9": $scope.selectedItemTaxYearLMR.startDate, //$scope.startTaxYearLMR,//start date 
                                        "10": $scope.selectedItemTaxYearLMR.endDate, //$scope.endTaxYearLMR,//end date 
                                        "11": $rootScope.userData.EntityData.data.caseID, //case id
                                        "12": $rootScope.userData.EntityData.data.caseID2, //case id 2
                                        "14": $rootScope.userData.EntityData.data.entityID, //leid
                                        "15": 'US_Tax', //$scope.selectedItemDepTypeLMR.value//dep type
                                        "0": 'US Tax'
                                    },
                                    "templateUrl": selectedObj.excelTemplateName
                                };
                                downloadRequestData.reportList.push(obj);
                                break;

                            case 8:
                                var obj = {};

                                obj = {
                                    "reportID": selectedObj.reportListID,
                                    "reportName": selectedObj.linkText,
                                    "reportShortName": selectedObj.outputName,
                                    "reportParams": {

                                        "103": $scope.selectedItemTaxYearLMR.startDate, //$scope.startTaxYearLMR,//start date 
                                        "104": $scope.selectedItemTaxYearLMR.endDate, //$scope.endTaxYearLMR,//end date 
                                        "100": $rootScope.userData.EntityData.data.caseID, //case id
                                        "101": $rootScope.userData.EntityData.data.caseID2, //case id 2
                                        "102": $rootScope.userData.EntityData.data.entityID, //leid
                                        "0": 'US Tax'

                                    },
                                    "templateUrl": selectedObj.excelTemplateName
                                };
                                downloadRequestData.reportList.push(obj);
                                break;



                            case 2:
                                var obj = {};

                                obj = {
                                    "reportID": selectedObj.reportListID,
                                    "reportName": selectedObj.linkText,
                                    "reportShortName": selectedObj.outputName,
                                    "reportParams": {
                                        "22": $scope.selectedItemTaxYearLMR.startDate, //$scope.startTaxYearLMR,//start date 
                                        "23": $scope.selectedItemTaxYearLMR.endDate, //$scope.endTaxYearLMR,//end date 
                                        "24": $rootScope.userData.EntityData.data.caseID, //case id
                                        "25": $rootScope.userData.EntityData.data.caseID2, //case id 2
                                        "26": $rootScope.userData.EntityData.data.entityID, //leid
                                        "27": 'US_Tax', //$scope.selectedItemDepTypeLMR.value//dep type
                                        "0": 'US Tax'
                                    },
                                    "templateUrl": selectedObj.excelTemplateName
                                };
                                downloadRequestData.reportList.push(obj);
                                break;

                            case 3:
                                var obj = {};
                                obj = {
                                    "reportID": selectedObj.reportListID,
                                    "reportName": selectedObj.linkText,
                                    "reportShortName": selectedObj.outputName,
                                    "reportParams": {
                                        "78": $rootScope.userData.EntityData.data.entityID, //leid//leid
                                        "76": $rootScope.userData.EntityData.data.caseID, //case id
                                        "77": $rootScope.userData.EntityData.data.caseID2, //case id 2
                                        "79": $scope.selectedItemTaxYearLMR.startDate, //start date
                                        "80": $scope.selectedItemTaxYearLMR.endDate, //end date
                                        "0": 'US Tax'
                                    },
                                    "templateUrl": selectedObj.excelTemplateName
                                };
                                downloadRequestData.reportList.push(obj);
                                break;
                        }
                    });
                    // category Three list end

                    //category Second list start
                    angular.forEach($scope.SelectedReports.categoryTwoList, function(selectedObj, key) {
                        switch (selectedObj.reportListID) {
                            case 17:
                                var obj = {};
                                obj = {
                                    "reportID": selectedObj.reportListID,
                                    "reportName": selectedObj.linkText,
                                    "reportShortName": selectedObj.outputName,
                                    "reportParams": {
                                        "2": $scope.selectedItemTaxYearTR.value, //taxyearId
                                        "3": $rootScope.userData.EntityData.data.caseID, //case id
                                        "4": $rootScope.userData.EntityData.data.caseID2, //case id 2
                                        "5": $rootScope.userData.EntityData.data.entityID, //leid
                                        "6": $scope.selectedItemDepTypeTR.value, //depType
                                        "63": $scope.selectedItemTaxYearTR.startDate, //$scope.startTaxYearLMR,//start date 
                                        "64": $scope.selectedItemTaxYearTR.endDate,
                                        "0": $scope.selectedItemDepTypeTR.display
                                    },
                                    "templateUrl": selectedObj.excelTemplateName
                                };
                                downloadRequestData.reportList.push(obj);
                                break;
                            case 7:
                                var obj = {};
                                obj = {
                                    "reportID": selectedObj.reportListID,
                                    "reportName": selectedObj.linkText,
                                    "reportShortName": selectedObj.outputName,
                                    "reportParams": {
                                        "87": $scope.selectedItemTaxYearTR.value, //$scope.selectedItemTaxYearTR.value, //taxyearId
                                        "82": $rootScope.userData.EntityData.data.caseID, //case id
                                        "83": $rootScope.userData.EntityData.data.caseID2, //case id 2
                                        "84": $rootScope.userData.EntityData.data.entityID, //leid
                                        "86": $scope.selectedItemDepTypeTR.value, //$scope.selectedItemDepTypeTR.value //depType
                                        "63": $scope.selectedItemTaxYearTR.startDate, //$scope.startTaxYearLMR,//start date 
                                        "64": $scope.selectedItemTaxYearTR.endDate,
                                        "0": $scope.selectedItemDepTypeTR.display
                                    },
                                    "templateUrl": selectedObj.excelTemplateName
                                };
                                downloadRequestData.reportList.push(obj);
                                break;
                            case 6:
                                var obj = {};
                                obj = {
                                    "reportID": selectedObj.reportListID,
                                    "reportName": selectedObj.linkText,
                                    "reportShortName": selectedObj.outputName,
                                    "reportParams": {
                                        "63": $scope.selectedItemTaxYearTR.startDate, //$scope.startTaxYearLMR,//start date 
                                        "64": $scope.selectedItemTaxYearTR.endDate, //$scope.endTaxYearLMR,//end date 
                                        "65": $rootScope.userData.EntityData.data.caseID, //case id
                                        "66": $rootScope.userData.EntityData.data.caseID2, //case id 2
                                        "67": $rootScope.userData.EntityData.data.entityID, //leid
                                        "68": $scope.selectedItemDepTypeTR.value, //dep type
                                        "0": $scope.selectedItemDepTypeTR.display
                                    },
                                    "templateUrl": selectedObj.excelTemplateName
                                };
                                downloadRequestData.reportList.push(obj);
                                break;
                        }

                    });
                    //category Second list end

                    $scope.getStartDateReconRep = function() {

                        if ($scope.reportObject.reconciliationReportSelecter == 'taxyear') {
                            return $scope.selectedItemTaxYearRR.startDate;
                        } else {
                            return $scope.reportObject.startTaxYearRR;
                        }
                    };
                    $scope.getEndDateReconRep = function() {
                        if ($scope.reportObject.reconciliationReportSelecter == 'taxyear') {
                            return $scope.selectedItemTaxYearRR.endDate;
                        } else {
                            return $scope.reportObject.endTaxYearRR;
                        }
                    };

                    //category One list start
                    angular.forEach($scope.SelectedReports.categoryOneList, function(selectedObj, key) {
                        switch (selectedObj.reportListID) {
                            case 10:
                                var obj = {};
                                obj = {
                                    "reportID": selectedObj.reportListID,
                                    "reportName": selectedObj.linkText,
                                    "reportShortName": selectedObj.outputName,
                                    "reportParams": {
                                        "69": $scope.getStartDateReconRep(), //$scope.startTaxYearLMR,//start date 
                                        "70": $scope.getEndDateReconRep(), //$scope.endTaxYearLMR,//end date 
                                        "71": $rootScope.userData.EntityData.data.caseID, //case id
                                        "73": $rootScope.userData.EntityData.data.caseID2, //case id 2
                                        "74": $rootScope.userData.EntityData.data.entityID, //leid
                                        "75": $scope.selectedItemDepTypeRR.value, //dep type
                                        "0": $scope.selectedItemDepTypeRR.display
                                    },
                                    "templateUrl": selectedObj.excelTemplateName
                                };
                                downloadRequestData.reportList.push(obj);
                                break;

                            case 4:
                                var obj = {};
                                obj = {
                                    "reportID": selectedObj.reportListID,
                                    "reportName": selectedObj.linkText,
                                    "reportShortName": selectedObj.outputName,
                                    "reportParams": {
                                        "31": $scope.getStartDateReconRep(), //$scope.startTaxYearRR,//Start date,
                                        "32": $scope.getEndDateReconRep(), //$scope.endTaxYearRR,//end date,
                                        "33": $rootScope.userData.EntityData.data.caseID, //case id
                                        "34": $rootScope.userData.EntityData.data.caseID2, //case id 2
                                        "36": $scope.selectedItemDepTypeRR.value, //depType
                                        "37": $rootScope.userData.EntityData.data.entityID, //leid
                                        "0": $scope.selectedItemDepTypeRR.display
                                    },
                                    "templateUrl": selectedObj.excelTemplateName
                                };
                                downloadRequestData.reportList.push(obj);
                                break;

                        }

                    });
                    //category one list end

                    Notification('Your reports will be ready shortly!');
                    ConsoleService.log("download request data ", downloadRequestData);

                    var downloadReportListPromise = ReportsDataService.makeMultipleFileDownloadRequest(downloadRequestData);
                    downloadReportListPromise.then(
                        function(payload) {

                            ConsoleService.log('payload', payload);
                            if (payload != null && payload.status) {
                                $scope.setIntervalCallsToCheckForCompletion(payload);
                                // $state.go("dashboard");
                            }
                        },

                        function(errorPayload) {
                            //LoadingService.removeLoader();
                            ConsoleService.error('payload', errorPayload);
                        });

                }

                $scope.startOverClicked = function() {
                    //$scope.selectedReportIdList= [];
                    $scope.$emit("startOverEvent", "");
                    $state.go("reports");
                }




                $scope.setIntervalCallsToCheckForCompletion = function(payload) {
                    ConsoleService.log('setIntervalCallsToCheckForCompletion', "interval starting");
                    var intervalObj = $interval(function() {
                        ConsoleService.log('setIntervalCallsToCheckForCompletion', "in interval");
                        $http.get(appSetting.ServerPath + "reports/getReportStatus?id=" + payload.requestID).then(function(response) {

                            if (response.data.status) {
                                try {
                                    if (response.data.data.createdFilePath != null && response.data.data.createdFilePath.length > 0) {

                                        ConsoleService.log('setIntervalCallsToCheckForCompletion-file downloaded', response.data);
                                        //LoadingService.removeLoader();
                                        $interval.cancel(intervalObj);
                                        var link = window.document.createElement("a");
                                        link.download = "Report";
                                        link.href = response.data.data.createdFilePath;
                                        //link.click();
                                        NotificationService.updateNotifications();
                                    }
                                } catch (ex) {
                                    ConsoleService.error('setIntervalCallsToCheckForCompletion', ex);
                                }
                            } else {
                                //LoadingService.removeLoader();
                                $interval.cancel(intervalObj);
                            }
                        }, function(err) {
                            ConsoleService.error('setIntervalCallsToCheckForCompletion', err);

                        })
                    }, 5000)
                }

            }
        ]);

    }());