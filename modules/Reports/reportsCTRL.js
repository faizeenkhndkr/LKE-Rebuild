﻿(
    function() {
        angular.module("MyLkeApp").controller("reportsCtrl", ["$scope", "$http", "appSetting",
            "$rootScope", "$state", "ReportsDataService", "LoginService", "LoadingService", "ConsoleService", "$filter",
            function($scope, $http, appSetting, $rootScope, $state, ReportsDataService, LoginService,
                LoadingService, ConsoleService, $filter) {
                $scope.selectedReportIdList = [];
                $scope.Reports = {}; // = ReportsDataService.getReportListData();
                $scope.someval = 90;

                $rootScope.activeTab1 = "";
                $rootScope.activeTab2 = "";
                $rootScope.activeTab3 = "active";
                $rootScope.activeTab4 = "";
                $rootScope.activeTab5 = "";
                $rootScope.activeTab6 = "";
                $rootScope.isAdminCollapse = false;

                if(!$rootScope.isLoggedIn){
                    $state.go("login");
                }


                $scope.getReportsForSelectedDatabase = function() {
                    LoadingService.showLoader();
                    var reportListPromise = ReportsDataService.getAllReportList();
                    reportListPromise.then(
                        function(payload) {
                            LoadingService.removeLoader();
                            if (payload != null) {
                                //lke management report
                                $scope.Reports.categoryOneList = $filter('filter')(payload.data, { reportCategoryID: 1, display: 1 }, true);
                                //tax report
                                $scope.Reports.categoryTwoList = $filter('filter')(payload.data, { reportCategoryID: 2, display: 1 }, true);
                                //reconciliation report
                                $scope.Reports.categoryThreeList = $filter('filter')(payload.data, { reportCategoryID: 3, display: 1 }, true);
                                ReportsDataService.setReportsData($scope.Reports);
                                ConsoleService.log("1 ", $scope.Reports.categoryOneList);
                                ConsoleService.log("2 ", $scope.Reports.categoryTwoList);
                                ConsoleService.log("3 ", $scope.Reports.categoryThreeList);
                            }
                        },

                        function(errorPayload) {
                            LoadingService.removeLoader();
                            ConsoleService.log('getReportsForSelectedDatabase', errorPayload);
                        });
                }



                try {
                    $scope.getReportsForSelectedDatabase();
                } catch (Ex) {

                    ConsoleService.log("ReportsCTRL ", Ex);


                }

                //databaseChangedBroadcaseEvent
                $scope.$on('databaseChangedBroadcaseEvent', function(event, data) {
                    //console.log(data); // 'Some data'
                    try {
                        $scope.getReportsForSelectedDatabase();
                    } catch (Ex) {

                    }
                });






                $scope.toggle = function(item, list) {
                    var idx = list.indexOf(item);
                    if (idx > -1) {
                        list.splice(idx, 1);
                    } else {
                        list.push(item);
                    }

                    //getting selected reports from previous screen
                    $scope.SelectedReports = ReportsDataService.getSelectedReportsObject($scope.selectedReportIdList);
                    ConsoleService.log('toggle', $scope.SelectedReports);


                    $scope.enableContinueBtn = false;
                    angular.forEach($scope.SelectedReports, function(item, key) {
                        ConsoleService.log('length', item.length);

                        if (item.length > 0) {
                            $scope.enableContinueBtn = true;
                        }
                    })

                };

                $scope.exists = function(item, list) {
                    return list.indexOf(item) > -1;
                };

                $scope.$on("startOverEvent", function(evt, data) {
                    $scope.selectedReportIdList = [];
                    $scope.SelectedReports = ReportsDataService.getSelectedReportsObject($scope.selectedReportIdList);

                });


                $scope.ContinueBtnClicked = function() {
                    $scope.enableContinueBtn = false;
                    $state.go("reports.details");
                }

            }
        ]);

    }());