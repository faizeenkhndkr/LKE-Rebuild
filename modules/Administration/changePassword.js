(
    function() {

        angular.module("MyLkeApp").controller("changePasswordCtrl", ["$scope", "$http", "appSetting",

            "uiGridConstants", "$state", "$rootScope", "ConsoleService", "LoginService", "$filter", "$q", "$mdDialog",
            function($scope, $http, appSetting,
                uiGridConstants, $state, $rootScope, ConsoleService, LoginService, $filter, $q, $mdDialog) {

                // $rootScope.activeTab1 = "";
                // $rootScope.activeTab2 = "active";
                // $rootScope.activeTab3 = "";
                // $rootScope.activeTab4 = "";
                // $rootScope.activeTab5 = "";
                // $rootScope.activeTab6 = "";

                $scope.successPasswordChanged = false;

                if (!$rootScope.isLoggedIn) {
                    $state.go("login");
                }

                $scope.LoginData = {};
                $scope.LoginData.username = JSON.parse(localStorage.getItem("userData")).username;

                $scope.changepassword = function() {
                    // if (($scope.password.length > 0) && ($scope.confirmpassword.length > 0)) {
                    $scope.islengthZero = false;
                    $http({
                            method: 'POST',
                            url: appSetting.ServerPath + 'api/UserLogin/ChangePassword',
                            data: $scope.LoginData,
                            headers: {
                                "Content-Type": "application/json"
                            }

                        }).then(function(response) {
                            $scope.successPasswordChanged = true;
                            // $state.go("login");

                            console.log(response);

                        })
                        // } else {
                        //     $scope.islengthZero = true;
                        // }
                }

                $scope.checkClicked = function() {
                    $scope.isClicked = false;
                }

                $scope.checkFocus = function() {
                    $scope.hjy = true;
                }
            }
        ]);

    }())