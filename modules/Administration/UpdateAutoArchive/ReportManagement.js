(
    function() {

        angular.module("MyLkeApp").controller("reportManagementCTRL", ["$scope", "$http", "appSetting", "$rootScope", "ConsoleService", "ReportsDataService", "LoadingService", "$filter", "$mdDialog", "$state", function($scope, $http, appSetting, $rootScope, ConsoleService, ReportsDataService, LoadingService, $filter, $mdDialog, $state) {


            $rootScope.activeTab1 = "";
            $rootScope.activeTab2 = "";
            $rootScope.activeTab3 = "";
            $rootScope.activeTab4 = "";
            $rootScope.activeTab5 = "";
            $rootScope.activeTab6 = "active";
            $rootScope.isCollapseAdmin = false;

            if (!$rootScope.isLoggedIn) {
                $state.go("login");
            }

            $scope.dbList123 = $rootScope.userData.dbList;
            $scope.selected = [];
            $scope.selectedDBList = [];
            $scope.firstPanel = true;
            $scope.secondPanel = false;
            $scope.items = $scope.dbList123;

            /*$http.get(appSetting.ServerPath + '/api/Administration/GetAllDatabasesList')
                .then(function (responseDataFull) {
                    $scope.dbList123 = responseDataFull.data.databaseList;
                    ConsoleService.log('getDatabasesList', responseDataFull);
            }, function (error) {
                    ConsoleService.error('getDatabasesList', error);
            });*/


            $scope.toggle = function(item, list) {
                ConsoleService.log("item", item);
                ConsoleService.log("list", list);
                var idx = list.indexOf(item);
                if (idx > -1) {
                    list.splice(idx, 1);
                } else {
                    list.push(item);
                }
                $scope.selectedDBList = list;

            };

            $scope.exists = function(item, list) {
                return list.indexOf(item) > -1;
            };
            $scope.toggleFirstPannel = function() {
                $scope.firstPanel = !$scope.firstPanel;
                $scope.secondPanel = false;
                if (!$scope.firstPanel) {
                    $scope.secondPanel = true;
                }
            }
            $scope.toggleSecondPannel = function() {
                $scope.secondPanel = !$scope.secondPanel;
                $scope.firstPanel = false;
                if (!$scope.secondPanel) {
                    $scope.firstPanel = true;
                }
            }
            $scope.initializeRefreshClientData = function() {
                $scope.obj.runYearAnd1 = "0";
                $scope.obj.runMatch = "1";
                $scope.obj.manualMatch = "0";
                $scope.obj.alternamteMatch = "1";
            }();
            $scope.selectedItemChangeTaxBook = function(item) {
                ConsoleService.log("TaxBook", item);
            }
            $scope.selectedItemChangeFromTaxYr = function(item) {
                ConsoleService.log("FromTaxYr", item);
            }
            $scope.selectedItemChangeToTaxYr = function(item) {
                ConsoleService.log("ToTaxYr", item);
            }
            $scope.refreshClientData = function(obj) {
                //$scope.initializeRefreshClientData();
                $http.post(appSetting.ServerPath + "api/Reports/RefreshData?deptype=" + obj.selectedTaxBook.value + "&yearid=" + obj.selectedFromTaxYr.value + "&Yearid1=" + obj.selectedToTaxYr.value + "&RunYearand1=" + obj.runYearAnd1 + "&RunMatch=" + obj.runMatch + "&ManualMatches=" + obj.manualMatch + "&AlternateMatch=" + obj.alternamteMatch + "&dbName=" + $rootScope.userData.SelectedDatabase.actualDbaseName)
                    .then(function(responseDataFull) {
                        if (responseDataFull.status) {
                            $scope.initializeRefreshClientData();
                        } else {
                            ConsoleService.error('refresh ClientData', 'success false');
                        }
                    }, function(error) {
                        ConsoleService.error('refresh ClientData failed', error);
                    });
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#refreshClientData')))
                    .clickOutsideToClose(true)
                    .title('REFRESH CLIENT DATA')
                    .textContent('The client data is getting refreshed. This may take a while, You will be intimated to your respective email id of the refreshed results.')
                    .ariaLabel('The client data is getting refreshed. This may take a while, You will be intimated to your respective email id of the refreshed results.')
                    .ok('OK')
                    .targetEvent('ev')
                );
            }
            $scope.refreshReports = function(selectedTaxYr, selected) {
                var updateArchiveObj = {};
                var selectedDBList = [];
                for (var i = 0; i < selected.length; i++) {
                    selectedDBList.push(selected[i].actualDbaseName);
                }
                updateArchiveObj = {
                    "dblist": selectedDBList,
                    "startDate": selectedTaxYr.startDate,
                    "endDate": selectedTaxYr.endDate,
                    "taxyearId": selectedTaxYr.value
                }
                ConsoleService.log("updateArchiveObj", updateArchiveObj);
                ConsoleService.log("selectedTaxYr", selectedTaxYr);
                ConsoleService.log("selected", selected);
                $http.post(appSetting.ServerPath + "api/Reports/UpdateArchive", updateArchiveObj)
                    .then(function(responseDataFull) {
                        if (responseData.status) {
                            $scope.selectedTaxYr = $scope.taxYearForRefresh[0];
                            $scope.selected = [];
                        } else {
                            ConsoleService.error('refreshing failed', 'success false');
                        }
                    }, function(error) {
                        ConsoleService.error('refreshing failed', error);
                    });
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#dialogContainer123')))
                    .clickOutsideToClose(true)
                    .title('REFRESH REPORTS')
                    .textContent('The reports are getting refreshed. This may take a while. You will be intimated about the result of the refresh to your respective email IDs.')
                    .ariaLabel('The reports are getting refreshed. This may take a while. You will be intimated about the result of the refresh to your respective email IDs.')
                    .ok('OK')
                    .targetEvent('ev')
                );

            }
            $scope.downloadYearsList = function() {
                LoadingService.showLoader();
                var _Promise = ReportsDataService.loadYearData();
                _Promise.then(
                    function(payload) {
                        LoadingService.removeLoader();
                        if (payload != null) {
                            $scope.fromTaxYear = payload.data;
                            $scope.toTaxYear = payload.data;
                            $scope.taxYearForRefresh = payload.data;
                            ConsoleService.log('$scope.fromTaxYear', $scope.fromTaxYear);
                            $scope.preSelectTaxYear($scope.fromTaxYear);
                        }
                    },
                    function(errorPayload) {
                        LoadingService.removeLoader();
                        ConsoleService.error('downloadYearsList', errorPayload);
                    });
            }();
            //download depreciation types
            $scope.downloadDepTypes = function() {
                LoadingService.showLoader();
                var _Promise = ReportsDataService.loadAllDepreciationTypes();
                _Promise.then(
                    function(payload) {
                        LoadingService.removeLoader();
                        if (payload != null) {
                            $scope.taxBook = payload.data;
                            ConsoleService.log('$scope.depreciationTypes', $scope.depreciationTypes);
                            $scope.preSelectDepType($scope.taxBook);
                        }
                    },
                    function(errorPayload) {
                        LoadingService.removeLoader();
                        ConsoleService.error('downloadDepTypes', errorPayload);
                    });
            }();

            $scope.preSelectDepType = function(depreciationTypeList) {
                try {
                    $scope.preSelectedDepreciationType = $filter('filter')(depreciationTypeList, { value: "US_Tax" })[0];
                    $scope.obj.selectedTaxBook = $scope.preSelectedDepreciationType;
                    ConsoleService.log('$scope.preSelectedDepreciationType', $scope.preSelectedDepreciationType);
                } catch (error) {
                    ConsoleService.error('preSelectDepType', error);
                }
            };
            $scope.preSelectTaxYear = function(yearsList) {
                try {
                    $scope.preSelectedTaxYear = $filter('filter')(yearsList, { display: (new Date().getFullYear()) - 1 })[0];
                    $scope.obj.selectedFromTaxYr = $scope.preSelectedTaxYear;
                    $scope.obj.selectedToTaxYr = $scope.preSelectedTaxYear;
                    $scope.selectedTaxYr = $scope.preSelectedTaxYear;
                    ConsoleService.log('$scope.obj.selectedFromTaxYr', $scope.obj.selectedFromTaxYr);
                } catch (error) {
                    ConsoleService.error('preSelectTaxYear', error);
                }
            };
            $scope.selectedItemChangeTaxYear = function(taxYr) {
                ConsoleService.log("taxYr", taxYr);
            }
        }]);

    }());