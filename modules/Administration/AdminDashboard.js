(
    function() {

        angular.module("MyLkeApp").controller("adminDashboardCtrl", ["$scope", "$rootScope", "$http", "appSetting",
            "uiGridConstants", "LoginService", "LoadingService", "$state", "ConsoleService", "$mdDialog", "$interval", "$timeout",
            "$filter", "$q",
            function($scope, $rootScope, $http, appSetting,
                uiGridConstants, LoginService, LoadingService, $state, ConsoleService, $mdDialog, $interval, $timeout, $filter, $q) {

                $rootScope.activeTab1 = "";
                $rootScope.activeTab2 = "";
                $rootScope.activeTab3 = "";
                $rootScope.activeTab4 = "";
                $rootScope.activeTab5 = "";
                $rootScope.activeTab6 = "active";
                $rootScope.isCollapseAdmin = true;

                $rootScope.adminAccess = true;


                if (!$rootScope.isLoggedIn) {
                    $state.go("login");
                }

                $scope.enableUpComingDeadLinesFilter = function() {
                    $scope.adminDashboardGrid.enableFiltering = !$scope.adminDashboardGrid.enableFiltering;
                    $scope.gridApi.core.clearAllFilters();
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                }

                $scope.adminDashboardGrid = {};

                $scope.adminDashboardGrid = {
                    enableColumnMenus: false,
                    paginationPageSizes: [25, 50, 75],
                    paginationPageSize: 25,
                    enableSorting: true,
                    enableFiltering: false,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
                    onRegisterApi: function(gridApi) {
                        $scope.gridApi = gridApi;

                    },
                };
                $scope.adminDashboardGrid.columnDefs = [

                    { name: 'USER NAME', displayName: 'USER NAME', field: 'webUserName', cellTemplate: '<div class="input-overflow-ellipsis" title="{{row.entity.webUserName}}" style="text-align:left;padding-left:4px; margin-top:5px;text-overflow: ellipsis; overflow: hidden;">{{row.entity.webUserName}}</div>' },
                    { name: 'FIRST NAME', displayName: 'FIRST NAME', field: 'firstName', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; padding-left:4px; margin-top:5px;">{{row.entity.firstName}}</div>' },
                    { name: 'LAST NAME', displayName: 'LAST NAME', field: 'lastName', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; padding-left:4px; margin-top:5px;">{{row.entity.lastName}}</div>' },
                    { name: 'EMAIL', id: 'emailAdmin', displayName: 'EMAIL', width: 250, field: 'email', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; padding-left:4px; margin-top:5px;">{{row.entity.email}}</div>' },
                    // { name: 'PHONE', displayName: 'PHONE', width: '105', field: 'phone', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:center; margin-top:5px;">{{row.entity.phone}}</div>' },
                    { name: 'CLIENTS', displayName: 'CLIENTS', enableFiltering: false, width: 150, field: 'clients', cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:left; margin-top:5px;padding-left:4px;">{{(row.entity.clients>1) ? row.entity.clients: row.entity.clientName}}</div>' },
                    {
                        name: 'LOCK',
                        displayName: ' LOCK',
                        field: ' ',
                        width: 70,
                        enableSorting: false,
                        enableFiltering: false,
                        cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:center; margin-top:3px;"><a ng-if="row.entity.isLocked==\'True\'|| row.entity.isLocked==\'true\'" ng-click="grid.appScope.lockedUser(row.entity,$event)"><md-tooltip md-direction="top">Unlock User</md-tooltip><img style="cursor:pointer"  src="./Images/locked.png" alt="Locked"></a><a ng-if="row.entity.isLocked == \'False\' || row.entity.isLocked == \'false\' || row.entity.isLocked == null"   ng-click="grid.appScope.unlockedUser(row.entity,$event)"><md-tooltip md-direction="top">Lock User</md-tooltip><img style="cursor:pointer"  src="./Images/unlocked.png" alt="Locked"></a>  </div>'
                    },
                    {
                        name: 'UPDATE',
                        displayName: 'UPDATE',
                        field: ' ',
                        width: 75,
                        enableSorting: false,
                        enableFiltering: false,
                        cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:center;  margin-top:3px;"><a  ng-click="grid.appScope.updateUser(row.entity)"><md-tooltip md-direction="top">Update User</md-tooltip><img style="cursor:pointer"  src="./Images/user.png" alt="Updated User"></a> </div>'
                    },
                    {
                        name: 'ACTIVE',
                        displayName: 'ACTIVE',
                        field: ' ',
                        width: 70,
                        enableSorting: false,
                        enableFiltering: false,
                        cellTemplate: '<div class="input-overflow-ellipsis" style="text-align:center; margin-top:3px;"><a ng-if="row.entity.inactive == \'True\' || row.entity.inactive == \'true\' || row.entity.inactive == 1" ng-click="grid.appScope.ActiveUser(row.entity,$event)"><md-tooltip md-direction="top">Activate User</md-tooltip><img style="cursor:pointer"  src="./Images/inactive-users.png" alt="Inactive User"></a><a ng-if="row.entity.inactive == \'False\' || row.entity.inactive == \'false\' || row.entity.inactive != 1" ng-click="grid.appScope.InActiveUser(row.entity,$event)"><md-tooltip md-direction="top">Inactivate User</md-tooltip><img style="cursor:pointer" src="./Images/active-users.png" alt="Active User"></a> </div>'
                    },
                ];

                /* Start:- For the Viewing controller data in Ui-Grid */

                $scope.getUIgridData = function() {
                    LoadingService.showLoader();
                    $http({
                        method: 'GET',
                        url: appSetting.ServerPath + 'api/Administration/GetAdminAllUsers'
                    }).then(function(response) {
                            var responseData = response.data.data;
                            $scope.adminDashboardGrid.data = responseData;
                            ConsoleService.log('AdministrationDashboardList', responseData);
                            LoadingService.removeLoader();
                            /* Start Filter Search */

                            $scope.refreshData = function(termObj) {
                                $scope.adminDashboardGrid.data = response.data.data;
                                while (termObj) {
                                    var oSearchArray = termObj.split(' ');
                                    $scope.adminDashboardGrid.data = $filter('filter')($scope.adminDashboardGrid.data, oSearchArray[0], undefined);
                                    oSearchArray.shift();
                                    termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
                                }
                            };


                            /* End Filter Search */
                        },
                        function(error) {
                            LoadingService.removeLoader();
                            $scope.adminDashboardGrid.data = data;
                        });

                }
                $scope.getUIgridData();

                /* End: Viewing controller here */

                /* Start: Roles Look-up */
                $scope.rolesLookUp = function() {
                    $http({
                        method: 'GET',
                        url: appSetting.ServerPath + 'api/Administration/GetAdminRolesLookup'

                    }).then(function(response) {
                            // console.log(JSON.stringify(response))
                            // return response;
                            $scope.rolesList = response.data.result;
                        },

                        function(error) {

                        });
                };

                $scope.rolesLookUp();
                /* End: Roles Look-up */


                $scope.updateUser = function(selectedRow) {

                    if (selectedRow == null) {

                        $scope.IsUsernameReadonly = false;
                        $scope.IsPasswordReadOnly = false;
                        $scope.updateBtn = false;

                        var confirm = $mdDialog.show({
                                scope: $scope,
                                controller: addUserDialogController,
                                preserveScope: true,
                                templateUrl: 'templates/addNewUser.html',
                                // parent: angular.element(document.body),
                                // targetEvent: ev,
                                clickOutsideToClose: true,
                                locals: {
                                    message: null,
                                }
                            })
                            .then(function(object) {
                                if (object.status == 200) {
                                    LoadingService.showLoader();
                                    $scope.getUIgridData();
                                }

                            }, function() {
                                LoadingService.removeLoader();
                                ConsoleService.log('Dialog', 'You cancelled the dialog.');
                            });

                    } else {
                        ConsoleService.log("firstRadioGroup", $scope.firstRadioGroup);
                        $scope.IsUsernameReadonly = true;
                        $scope.IsPasswordReadOnly = true;
                        $scope.updateBtn = true;
                        $http({
                            method: 'GET',
                            url: appSetting.ServerPath + 'api/Administration/GetDatabasesForUser?userid=' + selectedRow.id,
                            headers: {
                                "Content-Type": "application/json"
                            }

                        }).then(function(response) {
                            var databaseListFromDB = response.data.databaseList;
                            LoadingService.removeLoader();
                            if (databaseListFromDB != null) {
                                $scope.dbListFromDB = databaseListFromDB;
                                $scope.selectedList = databaseListFromDB;
                                ConsoleService.log("Selected List", $scope.selectedList);
                            }
                            var confirm = $mdDialog.show({
                                    scope: $scope,
                                    preserveScope: true,
                                    controller: addUserDialogController,
                                    templateUrl: 'templates/addNewUser.html',
                                    // parent: angular.element(document.body),
                                    // targetEvent: ev,
                                    clickOutsideToClose: true,
                                    locals: {
                                        message: selectedRow,

                                    }
                                })
                                .then(function(object) {
                                    if (object.status == 200) {
                                        LoadingService.showLoader();
                                        $scope.rolesLookUp();
                                        $scope.getUIgridData();
                                    }

                                }, function() {
                                    LoadingService.removeLoader();
                                    ConsoleService.log('Dialog', 'You cancelled the dialog.');
                                });

                        }, function(error) {
                            LoadingService.removeLoader();
                        })
                    }

                };

                function addUserDialogController($scope, $mdDialog, message, $rootScope) {
                    $scope.newUser = {};
                    $scope.firstRadioGroupDBList = [];
                    $scope.secondRadioGroupDBList = [];
                    $scope.thirdRadioGroupDBList = [];
                    $scope.fourthRadioGroupDBList = [];
                    $scope.fifthRadioGroupDBList = [];
                    $scope.hide = function() {
                        $mdDialog.hide();
                    };
                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };
                    if (message == null) {
                        $scope.newUser = {};
                        $scope.newUser.roleDesc = {};
                        $scope.dbListFromDB = null;
                        $scope.newUser.roleDesc = $scope.rolesList[2];
                    } else {
                        $scope.newUser = {};
                        $scope.newUser.roleDesc = {};
                        angular.copy(message, $scope.newUser);
                        for (var i = 0; i < $scope.rolesList.length; i++) {
                            if (message.roleDesc == $scope.rolesList[i].id) {
                                $scope.newUser.roleDesc = $scope.rolesList[i];
                            }
                        }
                        //   $scope.newUser.roleDesc = 2;
                        ConsoleService.log('messageNewUser', message);
                    }

                    /* Start: md-checkbox database list */

                    $scope.selected = [];
                    $scope.deletetedlist = [];
                    $scope.selectedList = [];
                    $scope.toggle = function(item, selectedList, state) {

                        var idx = selectedList.indexOf(item);
                        if (state) {
                            $scope.deletetedlist.push(item);
                        } else {
                            if (idx > -1) {
                                var found = selectedList[idx];

                                selectedList.splice(idx, 1);
                            } else {
                                selectedList.push(item);
                            }
                            $scope.selectedList = selectedList;
                        }

                    };

                    $scope.exists = function(item, selectedList) {
                        if ($scope.dbListFromDB) {
                            var dbList = $scope.dbListFromDB.filter(
                                function(obj) {
                                    return item.id == obj.dbaseID;
                                }
                            )
                            var result = (dbList.length > 0 ? true : false);
                        }
                        return result;
                    };


                    /* End: md-checkbox database list */



                    $scope.quesListArray = [{
                            id: "1",
                            que: "What is your mother's maiden name ?"
                        },
                        {
                            id: "2",
                            que: "What is your pet's name ?"
                        },
                        {
                            id: "3",
                            que: "What is your favorite place ?"
                        },
                        {
                            id: "4",
                            que: "What is your birth place ?"
                        }
                    ];
                    $scope.newUser.selectedSecQue = $scope.quesListArray[0].que;

                    //$scope.newUser.selectedRole = $scope.rolesListArray[0].role;
                    var spliceIncrement = 0;
                    $scope.eachColumnsLength = $rootScope.userData.dbList.length / 5;
                    for (var i = 1; i <= 5; i++) {
                        switch (i) {
                            case 1:
                                $scope.firstRadioGroupDBList = $rootScope.userData.dbList.slice(spliceIncrement, (i * $scope.eachColumnsLength));
                                // ConsoleService.log('test', $scope.firstRadioGroupDBList);
                                // $scope.test = $filter('filter')($scope.firstRadioGroupDBList, { actualDbaseName: $scope.newUser.clientName }, true)[0];
                                // $scope.test = true;
                                break;
                            case 2:
                                $scope.secondRadioGroupDBList = $rootScope.userData.dbList.slice(spliceIncrement, (i * $scope.eachColumnsLength));
                                break;
                            case 3:
                                $scope.thirdRadioGroupDBList = $rootScope.userData.dbList.slice(spliceIncrement, (i * $scope.eachColumnsLength));
                                break;
                            case 4:
                                $scope.fourthRadioGroupDBList = $rootScope.userData.dbList.slice(spliceIncrement, (i * $scope.eachColumnsLength));
                                break;
                            case 5:
                                $scope.fifthRadioGroupDBList = $rootScope.userData.dbList.slice(spliceIncrement, (i * $scope.eachColumnsLength));
                                break;
                        }

                        //alert(JSON.stringify($scope.firstRadioGroupDBList));
                        spliceIncrement = spliceIncrement + $scope.eachColumnsLength;
                    }

                    /* For Editing the UI-Grid Row */

                    $scope.addNewUsers = function(userData) {
                        ConsoleService.log("firstRadioGroup", $scope.firstRadioGroup);
                        $scope.addBtn = true;
                        LoadingService.showLoader();
                        // userData.selectedList = $scope.selectedList;
                        var Addclients = [];
                        var Deleteclients = [];
                        var finalObj = {};

                        angular.forEach($scope.selectedList, function(item, key) {
                            ConsoleService.log("addNewuser_selectedList", $scope.selectedList);
                            let client = {};
                            client.dbaseID = item.id;

                            ConsoleService.log("item list", item);
                            Addclients.push(client);
                        })
                        angular.forEach($scope.deletetedlist, function(item, key) {
                            ConsoleService.log("deleteUser", $scope.deletetedlist);
                            let client = {};
                            client.dbaseID = item.id;
                            client.iD = $filter('filter')($scope.dbListFromDB, { dbaseID: item.id }, true)[0].id;
                            ConsoleService.log("item list", item);
                            Deleteclients.push(client);
                        })
                        console.log(userData);

                        finalObj.webUser = userData;
                        finalObj.clients = Addclients;
                        finalObj.delclients = Deleteclients;
                        ConsoleService.log('finalObj.webUser', finalObj);
                        //return;
                        $http({
                            method: 'POST',
                            url: appSetting.ServerPath + 'api/Administration/AddEditUser',
                            data: finalObj,
                            headers: {
                                "Content-Type": "application/json"
                            }

                        }).then(function(response) {
                            $scope.getUIgridData();
                            //console.log(response);

                            $mdDialog.hide(response);
                            LoadingService.removeLoader();
                        })

                    };


                    /* End Editing the UI-Grid Row */

                    $scope.checkUsernameExist = function(username) {

                            $http({
                                method: 'GET',
                                url: appSetting.ServerPath + "api/Administration/usernameExist?username=" + username

                            }).then(function(response) {

                                    $scope.webUserName = response.data.result;
                                    $scope.newUser.isWebUserNameExist = response.data.result;
                                    ConsoleService.log("Valid username", response);
                                },

                                function(error) {

                                    ConsoleService.log("Invalid username", response);
                                });

                        }
                        // $scope.search = function(username) {
                        //     $http.get(appSetting.ServerPath + "api/Administration/usernameExist?username=" + $rootScope.userData.username).then(function(response) {
                        //         $scope.webUserName = response.data.result;
                        //     });
                        // };   

                };
                $scope.selectedRole = function() {
                    //alert(JSON.stringify($scope.newUser));
                }
                $scope.validatePhoneNumber = function(phoneNumber) {
                    $scope.newUser.phone = phoneNumber.replace(/[^0-9]/g, '');
                }
                $scope.userLockConfirm = function(isLocked, entityObj) {

                    $http({
                        method: 'GET',
                        url: appSetting.ServerPath + "api/Administration/LockUnLockUsers?wuid=" + entityObj.id + "&isLocked=" + isLocked
                    }).then(function(response) {

                            if (response.status == 200) {
                                $scope.isLocked = response.data.result;
                                ConsoleService.log("Valid Lock User", response);
                                $scope.getUIgridData();
                            }
                        },

                        function(error) {
                            $scope.isLocked = false;
                            $scope.getUIgridData();
                            ConsoleService.log("Invalid Lock User", response);
                        });
                    L

                };

                $scope.userActiveConfirm = function(inactive, entityObj) {

                    $http({
                        method: 'GET',
                        url: appSetting.ServerPath + "api/Administration/ActiveInactiveUsers?wuid=" + entityObj.id + "&inactive=" + inactive
                    }).then(function(response) {

                            if (response.status == 200) {
                                $scope.inactive = response.data.result;
                                ConsoleService.log("Active User", response);
                                $scope.getUIgridData();
                            }

                        },

                        function(error) {

                            $scope.inactive = false;
                            $scope.getUIgridData();
                            ConsoleService.log("Inactive User", response);
                        });

                };

                $scope.lockedUser = function(entityObj, ev) {

                    var confirm = $mdDialog.confirm()
                        .title('Do you want to unlock the user ?')
                        .targetEvent(ev)
                        .ok('Ok')
                        .cancel('cancel');

                    $mdDialog.show(confirm).then(function() {

                        $scope.userLockConfirm('False', entityObj);
                        $scope.getUIgridData();
                    }, function() {

                    });

                };

                $scope.unlockedUser = function(entityObj, ev) {
                    var confirm = $mdDialog.confirm()
                        .title('Do you want to lock the user ?')
                        .targetEvent(ev)
                        .ok('Ok')
                        .cancel('cancel');

                    $mdDialog.show(confirm).then(function() {

                        $scope.userLockConfirm('True', entityObj);
                        $scope.getUIgridData();

                    }, function() {

                    });

                };

                $scope.ActiveUser = function(entityObj, ev) {

                    var confirm = $mdDialog.confirm()
                        .title('Do you want to activate the user ?')
                        .targetEvent(ev)
                        .ok('Ok')
                        .cancel('cancel');

                    $mdDialog.show(confirm).then(function() {
                        LoadingService.showLoader();
                        $scope.userActiveConfirm('False', entityObj);

                    }, function() {

                    });
                    LoadingService.removeLoader();
                };

                $scope.InActiveUser = function(entityObj, ev) {
                    var confirm = $mdDialog.confirm()
                        .title('Do you want to inactivate the user ?')
                        .targetEvent(ev)
                        .ok('Ok')
                        .cancel('cancel');

                    $mdDialog.show(confirm).then(function() {

                        $scope.userActiveConfirm('True', entityObj);

                    }, function() {

                    });
                    LoadingService.removeLoader();
                };

            }
        ]);

    }());